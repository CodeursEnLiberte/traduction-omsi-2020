# Pays : Ouganda

# Auteur : Oliver Poole

# Organisation : Space for Giants

# Site web : www.spaceforgiants.org

# Titre : Attraper les braconnières et braconniers : l’IA pour la conservation de la faune sauvage

# Introduction

La pandémie de COVID-19 a été un signal d’alarme pour la société moderne. Alors
que la plupart des scientifiques pensent qu’elle a été causée par un transfert
zoonotique résultant d’un mélange des méthodes d’élevage contemporaines, de
l’exploitation des espèces par le biais du commerce illégal d’espèces sauvages
(avec une transmission possible par un pangolin) et les conditions des marchés
aux bestiaux surpeuplés et insalubres, elle peut apparaitre comme un symptôme
d’un déséquilibre entre des comportements contemporains et un mode de vie plus
naturel.[^1] Cette situation a été aggravée par l’utilisation des réseaux
sociaux pour diffuser de fausses informations sur le virus, entraînant le risque
que la technologie soit perçue comme faisant partie du problème plutôt que de la
solution à la question de la durabilité.[^2]

Ce rapport, cependant, mettra en lumière une façon dont une avancée
technologique sophistiquée, en l’occurrence l’intelligence artificielle (IA),
est exploitée pour défendre la nature. En prenant pour exemple l’Ouganda, État
d’Afrique de l’Est, il montrera comment – afin de protéger la vie sauvage contre
celles et ceux qui souhaitent l’exploiter à des fins lucratives – des solutions
contemporaines de pointe ont été adoptées pour faire face à la crise du
braconnage, tout en protégeant des paysages clés et en permettant le
développement de modèles économiques durables pour les communautés locales. Bien
qu’il reste encore beaucoup à faire pour en tirer tous les avantages potentiels,
il s’agit d’un exemple important de la façon dont les dernières innovations
technologiques peuvent défendre notre relation avec la nature, et non
l’affaiblir.

# Le commerce illégal d’animaux sauvages

Le commerce illégal très sophistiqué des espèces sauvages et de leurs produits
dérivés met en danger des espèces dans le monde entier. Il s’agit du quatrième
crime transnational le plus rentable après le trafic de drogue, le trafic
d’armes et la traite des êtres humains, et il représente entre 7 et 23 milliards
de dollars par an[^3]. Il est souvent dirigé par des réseaux criminels bien
organisés qui cherchent à exploiter les gains élevées et les faibles risques de
ce commerce. Il sape les efforts environnementaux, alimente la corruption,
menace l’état de droit et nuit aux communautés qui dépendent du tourisme lié à
la faune sauvage[^4].

La demande de produits de la faune sauvage est souvent alimentée par les
croyances en leur valeur médicinale ou le statut social qui leur est associé.
Dans d’autres cas, elle est motivée par le désir de posséder des animaux de
compagnie exotiques ou des plantes et animaux rares. Au niveau local, le
braconnage est également le résultat de la pauvreté, de la corruption et de
l’instabilité politique. Dans tous les cas, le braconnage, le commerce et la
consommation illicites d’espèces sauvages constituent l’une des menaces les plus
destructrices et déstabilisantes pour la conservation[^5].

Son impact sur les populations mondiales d’éléphants et de rhinocéros fait
maintenant l’objet d’une attention internationale, mais d’autres mammifères sont
soumis à une pression tout aussi forte. Il s’agit notamment des félins – comme
les lions, les tigres et les léopards des neiges – et des primates, y compris
les grands singes. De nombreuses espèces de reptiles, d’oiseaux, d’amphibiens,
de poissons et d’invertébrés nécessitent également une action urgente pour leur
protection[^6]. Le pangolin, mammifère à la peau écailleuse recherché pour sa
viande et ses écailles et qui a pu être un vecteur zoonotique du COVID-19, est
considéré comme le mammifère faisant l’objet du plus important trafic illégal au
monde, les braconnières et braconniers ayant tué environ un million de pangolins
africains au cours de la dernière décennie pour leur viande, un mets délicat
dans certaines régions d’Asie, et pour leurs écailles de kératine, un ingrédient
de la médecine traditionnelle chinoise[^7].

L’Ouganda est l’une des nations dont la faune a été particulièrement touchée.
Dans les années 1960, le pays comptait plus de grands herbivores tels que les
éléphants et les hippopotames par kilomètre carré que tout autre pays africain.
Dans les années 1980, la seule population d’éléphants avait été réduite à
environ 700 ou 800 individus, bien que les efforts de conservation ont depuis
permis de remonter leur nombre à environ 5 000[^8]. L’Ouganda est également une
importante voie de transit pour les espèces sauvages illégales et leurs produits
dérivés, dont la plupart sont introduits clandestinement depuis la République
Démocratique du Congo. Cela a entraîné l’apparition d'organisations criminelles
axées sur le commerce, notamment de l’ivoire et des pangolins[^9].

Cette situation est importante non seulement pour des raisons de conservation,
mais aussi pour des raisons sociales et économiques. Jusqu’à l’impact actuel du
COVID-19 sur le secteur du tourisme, le nombre de touristes en Afrique devait
passer de 62 millions en 2016 à 134 millions en 2030[^10]. Quatre touristes sur
cinq qui viennent le font pour découvrir la vie sauvage[^11]. Même après le
COVID, une forte augmentation est toujours prévue, notamment parce que l’on
s’attend à ce que les gens recherchent désormais une expérience de vacances
davantage tournée vers la nature[^12]. En réponse, l’Ouganda a travaillé
activement au développement de son produit de tourisme animalier, et les
communautés locales autour de ses parcs nationaux peuvent potentiellement
bénéficier économiquement d’un secteur florissant du tourisme animalier, dans le
contexte d’opportunités d’emplois traditionnellement mal payés dans ces
zones[^13]. C’est pourquoi la menace qui pèse sur la faune et la flore du pays
constitue aussi une menace pour les objectifs de développement du pays et de ces
communautés.

# Lutter contre le braconnage

L’un des plus grands défis auxquels sont confronté·e·s les défenseur·e·s de la
nature est que celles et ceux qui braconnent semblent souvent avoir une longueur
d’avance sur leurs efforts, en raison de la dispersion naturelle des populations
d’espèces et du nombre limité de gardes-faune que les contraintes budgétaires
actuelles permettent d’employer. La technologie est une solution pour combler ce
manque, et l’Ouganda a été le pionnier de deux des solutions les plus innovantes
et les plus importantes dans ce domaine : SMART et PAWS. Toutes deux se sont
avérées efficaces pour donner aux garde-faunes un avantage sur les braconnières
et braconniers, et les essais réalisés en Ouganda ont abouti à l’adoption de ces
deux solutions dans d’autres pays confrontés à des défis similaires.

# *SMART*

SMART est l’acronyme de Spatial Monitoring and Reporting Tool (outil de
surveillance spatiale et d’analyse) et est une solution à code source
ouvert[^14]. Il s’agit d’un logiciel accessible et puissant pour gérer les
données relatives à l’application de la loi. Il fonctionne grâce aux
garde-faunes sur le terrain qui collectent des données au cours de leurs
patrouilles quotidiennes. Ces données peuvent ensuite être analysées par
ordinateur afin de comprendre les tendances et les points chauds du braconnage.
Les données recueillies sont nombreuses et comprennent des éléments tels que la
localisation des animaux, les preuves de braconnage telles que la pose de
collets, et toute arrestation pour activités illégales. Ces données sont
enregistrées par les garde-faunes à l’aide d’un appareil portatif ou, si le
nombre d’appareils disponibles est insuffisant, à l’aide d’un papier et d’un
stylo, et sont intégrées une fois de retour à la base.

Les données sont ensuite introduites dans un ordinateur central auquel on peut
poser des questions spécifiques telles que : Où sont allés mes garde-faunes ?
Combien de patrouilles à pied ont abouti à l’arrestation de personnes qui
braconnaient ? Ou encore, où les carcasses ont-elles été enregistrées ? Les
informations sont converties en cartes, graphiques et rapports visuellement
informatifs – par exemple, pour montrer les lieux d’observation des carcasses et
les tendances de leur taux de détection. Ces données sont ensuite corrigées de
tout biais involontaire causé par la fréquence à laquelle une zone spécifique
est balayée par les patrouilles. Une zone visitée plus souvent donnera
probablement lieu à une plus grande concentration de données, mais cela ne
signifie pas qu’il s’agit nécessairement du point où le braconnage est le plus
probable. De même, une zone peu visitée produira probablement peu de données,
mais peut néanmoins être une zone où le braconnage est en fait en hausse. Cette
correction permet donc d’identifier des tendances inhabituelles et d’alerter sur
une activité isolée mais significative. Les gestionnaires de la conservation
peuvent ainsi enregistrer plus efficacement les données et analyser
rétrospectivement l’impact des patrouilles.[^15]

Le système a été développé par un partenariat international d’organisations de
conservation. Il s’agit de la Convention sur le commerce international des
espèces de faune et de flore sauvages menacées d’extinction CITES (Convention on
International Trade in Endangered Species of Wild Fauna and Flora), du programme
de surveillance de l’abattage illégal d’éléphants MIKE (Monitoring the Illegal
Killing of Elephants), de la Société zoologique de Francfort (Frankfurt
Zoological Society), de la Conservation mondiale de la Nature (Global Wildlife
Conservation), du Zoo de Caroline du Nord (North Carolina Zoo), de Panthera, de
la Fondation Peace Parks (Peace Parks Foundation), de la Société de conservation
de la nature (Wildlife Conservation Society), du Fonds mondial de la nature
(World Wildlife Fund) et de la Société zoologique de Londres (Zoological Society
of London).[^16]

Le système a été testé à l’origine dans le parc national Queen Elizabeth en
Ouganda, l’une des zones protégées les plus importantes du pays pour la
conservation des éléphants, mais aussi une zone qui avait particulièrement
souffert du braconnage[^17]. C’est l’accumulation de données à cet endroit qui a
rendu son impact si significatif. Une fois mis en œuvre, sur une période de
12 ans, la détection d’activités illégales telles que le braconnage et
l’empiètement de bétail a augmenté de 250 %, sans augmentation du nombre de
garde-faunes déployé·e·s[^18]. En effet, le succès de ce projet a été tel que
l’Autorité de la faune sauvage d’Ouganda a étendu son utilisation à l’ensemble
de son réseau de zones protégées.

L’essai ougandais a également conduit à sa mise en œuvre sur 147 autres sites
dans le monde. Les agences chargées des aires protégées et de la faune sauvage
de sept pays se sont maintenant engagées à suivre l’Ouganda et à le mettre en
œuvre dans l’ensemble de leurs réseaux d’aires protégées. Il s’agit du Belize,
du Bhoutan, de la Colombie, du Gabon, de Madagascar, du Pérou et de la
Thaïlande[^19]. Dans tous ces endroits, ils ont également constaté que le
système permettait aux gestionnaires de la conservation de coordonner plus
efficacement leurs efforts de protection.

# *PAWS*

PAWS est l’acronyme de Protection Assistant for Wildlife Security (assistant de
protection pour la sécurité de la faune sauvage) et est un assistant de
protection fondé sur la théorie des jeux[^20]. La mise en œuvre réussie de SMART
en Ouganda a permis à ce pays d’être le premier dans lequel – suite à des
recherches commencées en 2013 – PAWS a été testé en 2014 puis à nouveau en
2016[^21]. Le programme SMART signifiait qu’il y avait déjà une accumulation de
données utilisables par cette nouvelle approche fondée sur l’IA, qui a été
développée par des académies de sciences appliquées dans des institutions telles
que Harvard et l’Université de Californie du Sud.

La théorie des jeux est l’étude de la prise de décision stratégique. Elle s’est
révélée particulièrement instructive dans la lutte contre le braconnage car,
dans ce jeu, il y a deux joueuses ou joueurs dont les objectifs sont
radicalement opposés et qui agissent tous deux logiquement dans leur propre
intérêt. Par exemple, si les garde-faunes empruntent chaque jour les mêmes
itinéraires de patrouille, les braconnières et braconniers réussiront en se
déplaçant simplement ailleurs. Par conséquent, il est dans l’intérêt des
garde-faunes de se comporter de manière aléatoire, mais ils ne veulent pas se
comporter de manière totalement aléatoire, car sinon ils pourraient ne pas aller
là où les braconnières et braconniers sont susceptibles de se trouver.
Idéalement, en décidant des itinéraires, les garde-faunes veulent dissuader les
braconnières et braconniers d’aller dans les endroits où il y a beaucoup
d’animaux en patrouillant régulièrement. De même, les braconnières et
braconniers devraient idéalement être dissuadés d’opérer dans les zones où il y
a moins d’animaux, car ils savent non seulement que les chances d’attraper un
animal y sont faibles, mais aussi qu’il existe un risque de patrouille surprise.
C’est en tenant compte de toutes ces variables (y compris des facteurs tels que
le terrain et la météo) que PAWS a aidé à déterminer les itinéraires quotidiens
optimaux que les garde-faunes disponibles devraient emprunter.

SMART permet d’évaluer plus efficacement l’impact des patrouilles, mais il
n’aide pas à créer des itinéraires de patrouille ou à identifier les cibles à
protéger. C’est toujours un humain – le ou la responsable de la patrouille – qui
s’en charge, et les humains ont du mal à générer des plannings crédibles qui
soient également imprévisibles. Nous sommes instinctivement attiré·e·s par les
modèles préexistants. PAWS, en revanche, s’appuie sur SMART et propose une
approche automatisée qui a permis d’établir des itinéraires de patrouille
beaucoup plus efficaces et aléatoires.

L’essai mené au parc national Queen Elizabeth a révélé que les patrouilles
assistées par PAWS ont obtenu de meilleurs résultats que les patrouilles
traditionnelles, autant pour les activités humaines que pour les animaux vus par
kilomètre parcouru[^22]. Grâce à PAWS, par exemple, l’équipe de mise en œuvre a
identifié un point névralgique du braconnage où les garde-faunes n’avaient
jamais patrouillé auparavant. En arrivant dans la zone, elles et ils ont
découvert un éléphant dont les défenses avaient été coupées ainsi qu’un piège
caché à proximité. Lors de tests ultérieurs, dix autres pièges à antilopes ont
été découverts avant qu’aucun animal n’ait été blessé ou tué[^23]. En fait, le
projet pilote a connu un tel succès que son utilisation a été étendue à un
deuxième parc national ougandais – Murchison Falls – en 2017 avant d’être
étendue à un parc au Cambodge en 2019. Aujourd’hui, grâce au soutien apporté au
projet par Microsoft AI, une version améliorée s’appuyant sur les enseignements
tirés des essais ougandais et cambodgiens devrait être lancée dans 10 à 20
autres parcs[^24]. De plus en plus, l’IA va contribuer à l’échelle mondiale à
faire en sorte que les gardes-faune puissent avoir le dessus sur les
braconnières et braconniers qui s’attaquent à la faune menacée de notre planète.

# Conclusion

L’approche SMART et PAWS adoptée en Ouganda constitue un exemple de réponse
concrète à la crise environnementale actuelle et fournit une solution qui a eu
un impact sur le braconnage dans le pays. Il s’agit donc d’un exemple clair et
mesurable de technologie apportant un changement positif.

Cependant, ce rapport est publié à un moment unique en raison de la pandémie de
COVID-19. Les yeux du monde étant tournés ailleurs, ceux qui s’attaquent aux
espèces sauvages menacées ont exploité les perturbations causées par le virus.
Les animaux en voie de disparition sont menacés car les limitations de
déplacements gênent les garde-faunes et les défenseur·e·s de la nature, et
l’effondrement soudain des financements causé par les conséquences économiques
de la pandémie met en péril l’avenir des programmes de protection[^25]. Le
tourisme s’étant également effondré, les revenus qui finançaient la protection
de la faune ont disparu et les adeptes du braconnage ont été encouragé·e·s par
l’absence de visiteuses et visiteurs[^26]. Les communautés locales, confrontées
à la pauvreté, en sont parfois réduites à tuer des animaux sauvages pour
survivre[^27].

En juillet 2020, le chef de l’Autorité de la faune sauvage d’Ouganda, Sam
Mwandha, a averti que les réseaux criminels impliqués dans le commerce illégal
de la faune sauvage exploitaient la situation du COVID-19 pour augmenter le
braconnage. Au moment même où il s’exprimait, l’Ouganda a annoncé qu’un éléphant
avait été tué par un collet dans le parc national de Murchison Falls par des
braconnières et braconniers qui voulaient son ivoire. De mars à avril, 822
pièges posés par des braconnières et braconniers pour piéger la faune sauvage
ont été découverts dans le parc de Bwindi en Ouganda, contre seulement 21 à la
même période l’année dernière, soit une augmentation de 3 814 %. Mwandha a
déclaré aux médias qu’à l’ère du COVID-19, « des moyens financiers sont
nécessaires pour lutter contre le braconnage, l’empiètement et le commerce
illégal d’espèces sauvages »[^28].

Le défi auquel sont confrontés les parcs nationaux comme ceux de l’Ouganda va
donc probablement s’amplifier. Une partie de la solution consistera à obtenir
des fonds pour soutenir le travail de l’autorité nationale chargée de la faune
sauvage et des ONG de conservation opérant dans le pays, afin que les
garde-faunes puissent continuer à faire leur travail. Mais une autre partie
consistera également à utiliser l’esprit d’innovation qui a produit SMART et
PAWS pour développer de nouvelles solutions. Nous devons de toute urgence
continuer à innover pour créer de nouveaux partenariats avec l’industrie, les
pouvoirs publics et les universités afin de développer de nouvelles réponses
technologiques. Les partenariats technologiques ont le potentiel de transformer
le domaine de la conservation de la faune et de la flore sauvages, en permettant
aux défenseurs de la nature de cibler les ressources de manière plus efficace et
plus efficiente et d’accroître leur impact. En 2020, une telle approche est plus
que jamais nécessaire.

# Mesures à prendre

# Les mesures suivantes doivent être prises en Ouganda :

- Les ONG en Ouganda doivent s’adresser aux entreprises technologiques pour
  s’assurer de nouvelles innovations technologiques dans ce domaine. Un moyen
  serait d’organiser une conférence numérique d’une journée pour que les
  défenseur·e·s de l’environnement et les représentant·e·s de ces entreprises
  puissent interagir et discuter.

- Les organisations de la société civile en Ouganda doivent évaluer de toute
  urgence les besoins humanitaires des communautés locales situées à proximité
  des zones protégées et créer une base de données informatique actualisée des
  lieux où la nourriture manque, afin d’identifier les besoins urgents et de
  limiter la mesure dans laquelle les gens se tournent vers la chasse au gibier
  par nécessité.

- Les ONG doivent faire pression sur les gouvernements occidentaux pour qu’ils
  soient conscients de l’impact de la pandémie de COVID-19 et de ses
  conséquences pour le tourisme sur les communautés locales, et pour obtenir des
  interventions en faveur de solutions à long terme – notamment en payant pour
  que des représentant·e·s des communautés locales soient formé·e·s pour devenir
  des collectrices et collecteurs de données pour SMART, et donc des
  « gardien·ne·s de la conservation » locaux.

- L’extension de PAWS grâce au soutien apporté au projet par Microsoft AI offre
  une occasion importante de s’engager davantage. Les organisations de la
  société civile devraient travailler avec l’Autorité de la faune sauvage
  d’Ouganda pour assurer la formation technique des ressortissantes locales et
  des ressortissants locaux afin qu’ils puissent entreprendre le travail
  technique nécessaire plutôt que d’employer des ressortissant·e·s
  étranger·ère·s pour le faire.

[^1]: UK Research and Innovation. (14 avril 2020). Where did the new
coronavirus come from?
<https://coronavirusexplained.ukri.org/en/article/cad0006>

[^2]: EBRD. (15 juin 2020). Is technology in the era of Covid-19 a
threat to democracy?
<https://www.ebrd.com/news/2020/is-technology-in-the-era-of-covid19-a-threat-to-democracy.html>

[^3]: <https://www.thegef.org/topics/illegal-wildlife-trade>

[^4]: <https://www.worldwildlife.org/threats/illegal-wildlife-trade>

[^5]: USAID. (2017). *What Drives Demand For Wildlife?*
<https://www.usaidwildlifeasia.org/resources/reports/inbox/what-drives-demand-for-wildlife>

[^6]: Cookson, C. (3 octobre 2019). Global wildlife trade a key factor
in species decline. *Financial Times*.
<https://www.ft.com/content/f2f48da6-e513-11e9-b112-9624ec9edc59>

[^7]: <https://www.traffic.org/what-we-do/species/pangolins>

[^8]: Pandey, A. (18 août 2015). Ugandan elephants\' long march to
recovery. *DW*.
<https://www.dw.com/en/ugandan-elephants-long-march-to-recovery/a-18655456>

[^9]: Rossi, A. (2018). *Uganda Wildlife Trafficking Assessment*.
TRAFFIC.
<https://www.traffic.org/publications/reports/uganda-wildlife-trafficking-assessment>

[^10]: Signé, L. (2018). *Africa\'s tourism potential: Trends, drivers,
opportunities, and strategies*. Brookings Institution.
<https://www.brookings.edu/wp-content/uploads/2018/12/Africas-tourism-potential_LandrySigne1.pdf>

[^11]: Space for Giants. (2019). *Building A Wildlife Economy*.
<https://spaceforgiantstest.squarespace.com/s/Building-Africas-Wildlife-Economy-Space-for-Giants-Working-Paper-1.pdf>

[^12]: Derrick, F. (10 juillet 2020). Wellness travel: Why it could be the
post-coronavirus stress-buster you need. *Skyscanner*.
<https://www.skyscanner.net/news/wellness-travel-coronavirus-stress-buster>

[^13]: Ledger, E. (5 octobre 2017). How tourism can safeguard African
wildlife. *The Independent*.
<https://www.independent.co.uk/voices/campaigns/GiantsClub/Uganda/how-tourism-can-safeguard-african-wildlife-a7985141.html>

[^14]: Huger, J. (20 juin 2013). Open source spatial monitoring gets
SMART for conservation. *Opensource.com.*
<https://opensource.com/life/13/6/SMART>

[^15]: <https://smartconservationtools.org>

[^16]: <https://www.zsl.org/conservation/how-we-work/conservation-technology/implementing-the-smart-approach>

[^17]: University of York. (17 août 2016). Poaching patrol: new
ranger methods decrease illegal activities.
<https://www.york.ac.uk/biology/news-events/news/2016/poachingpatrolnewrangermethodsdecreaseillegalactivities>

[^18]: Harfenist, E. (20 août 2016). New Tech Increases Detection Of
Illegal Acts In Protected Areas. *Vocativ*.
<https://www.vocativ.com/352526/new-tech-increases-detection-of-illegal-acts-in-protected-areas/index.html>

[^19]: Montefiore, A. (15 mars 2016). The Spatial Monitoring and
Reporting Tool (SMART). *WILDLABS*.
<https://www.wildlabs.net/resources/case-studies/spatial-monitoring-and-reporting-tool-smart>

[^20]: <https://sc.cs.cmu.edu/research-detail/102-protection-assistant-for-wildlife-security>

[^21]: Ibid.; Zewe, A. (11 octobre 2019). Artificial intelligence helps
rangers protect endangered wildlife. *Phys.org*.
<https://phys.org/news/2019-10-artificial-intelligence-rangers-endangered-wildlife.html>

[^22]: Synced (19 octobre 2019). AI In Wildlife Conservation. *Synced*.
<https://syncedreview.com/2019/10/19/ai-in-wildlife-conservation>

[^23]: Zewe, A. (11 octobre 2019). Op. cit.

[^24]: Ibid.

[^25]: Wildlife and Countryside Link. (2020). *Environment and
Conservation Organisations Coronavirus Impact Survey Report.*
<https://www.heritagefund.org.uk/sites/default/files/media/attachments/Coronavirus%20eNGO%20survey%20analysis%20report_1.pdf>

[^26]: Greenfield, P., & Muiruri, P. (5 mai 2020). Conservation in
crisis: ecotourism collapse threatens communities and wildlife. *The
Guardian*.
<https://www.theguardian.com/environment/2020/may/05/conservation-in-crisis-covid-19-coronavirus-ecotourism-collapse-threatens-communities-and-wildlife-aoe>

[^27]: Matthews, A. (21 mai 2020). The wild animals at risk in
lockdown. *BBC*.
<https://www.bbc.com/future/article/20200520-the-link-between-animals-and-covid-19>

[^28]: Ledger, E. (20 août 2020). The 'catastrophic' conservation
emergency left in Covid's wake. *The Independent*.
<https://www.independent.co.uk/news/world/coronavirus-catasrophic-conservation-emergency-illegal-wildlife-trade-a9619901.html>
