Auteur : Abebe Chekol

Organisation: Internet Society (ISOC) -- Chapitre éthiopien

E-mail: <abechekol@yahoo.com>

Pays : Éthiopie

# Technologies climatiques en Éthiopie : vers une économie verte résiliente au dérèglement climatique

## Introduction

L’Éthiopie est l’économie à la croissance la plus rapide de la région et, avec
plus de 112 millions d’habitants (en 2019)[^1], la deuxième nation la plus
peuplée d’Afrique après le Nigeria. La stratégie gouvernementale pour une
économie verte résiliente face au changement climatique (CRGE), adoptée en 2011,
vise à faire de l’Éthiopie un pays à revenu intermédiaire d’ici 2025, résilient
aux impacts du dérèglement climatique et sans augmentation nette des émissions
de gaz à effet de serre (GES) par rapport aux niveaux de 2010. La stratégie
repose sur quatre piliers : la réduction des émissions agricoles, la protection
et l’expansion des forêts, le développement de la production d’électricité
renouvelable et l’adoption de technologies à haut rendement énergétique dans les
transports, l’industrie et les zones urbanisées[^2].

Les dirigeant·e·s politiques du monde entier sont conscient·e·s de la nécessité
d’une action immédiate et efficace pour répondre au dérèglement climatique. Ces
réponses comprennent des actions visant à réduire les émissions de GES ainsi que
des initiatives d’adaptation pour réduire la vulnérabilité de la population et
de l’économie aux effets du dérèglement climatique.

À cet égard, en mars 2017, l’Éthiopie a ratifié l’accord de Paris, qui est un
accord de 2016 dans le cadre de la Convention-cadre des Nations unies sur les
changements climatiques (CCNUCC) traitant de l’atténuation des émissions de GES,
de l’adaptation et du financement. Le jour où elle a ratifié l’accord de Paris,
l’Éthiopie a également soumis sa contribution prévue déterminée au niveau
national (CPDN), la transformant ainsi en CDN. Tirée de sa stratégie CRGE, la
CDN de l’Éthiopie vise à limiter les émissions nettes de GES du pays en 2030 à
145 mégatonnes (Mt) eqCO₂[^3] ou moins, ce qui réduirait les émissions du
scénario du statu quo d’environ 64% et représenterait une réduction absolue des
émissions de 5 Mt eqCO₂ par rapport aux émissions de 2010, selon la CDN. Cela
signifie que l’Éthiopie vise à abaisser le niveau de GES de 1,8 en 2010 à 1,1 en
2030[^4].

La CDN de l’Éthiopie en 2017 a divisé les secteurs d’émissions en six
catégories : l’agriculture (bétail et sol), la sylviculture, le transport,
l’industrie (y compris l’exploitation minière), l’électricité et les bâtiments
(y compris les déchets et les villes vertes). Elle prévoit une augmentation
significative des émissions jusqu’en 2030 dans presque tous les secteurs, les
trois plus grands contributeurs étant l’agriculture, la sylviculture et les
transports.[^5]

Ce rapport examine donc les technologies climatiques en Éthiopie et leur rôle
dans la réduction des émissions de gaz à effet de serre. Il présente des
exemples choisis dans les trois domaines cités plus haut et met en évidence les
actions politiques susceptibles de renforcer la contribution des technologies
climatiques pour permettre au pays d’atteindre ses objectifs d’atténuation.

## Le dérèglement climatique et le contexte numérique

La contribution actuelle de l’Éthiopie à l’augmentation mondiale des émissions
de GES depuis la révolution industrielle est pratiquement insignifiante. Même
après des années de croissance économique rapide, les émissions actuelles par
habitant, inférieures à 2 t eqCO₂, sont modestes par rapport aux plus de 10 t
par habitant en moyenne dans l’Union européenne et aux plus de 20 t par habitant
aux États-Unis et en Australie. Globalement, les émissions totales de
l’Éthiopie, qui s’élèvent à environ 150 Mt eqCO₂, représentent moins de 0,3% des
émissions mondiales[^6].

Sur les 150 Mt eqCO₂ en 2010, plus de 85% des émissions de GES proviennent des
secteurs de l’agriculture (50%) et de la sylviculture (37%). Ils sont suivis par
les transports, l’électricité, l’industrie et le bâtiment, qui ont contribué à
hauteur de 3% chacun[^7].

Dans le secteur des technologies numériques, la politique et la stratégie
nationales de l’Éthiopie en matière de technologies de l’information et de la
communication (TIC) (2016), vise, entre autres, à utiliser les TIC pour atténuer
le dérèglement climatique, ainsi que pour la communication d’urgence et les
secours en cas de catastrophe[^8]. Le pays a fait preuve d’une certaine
croissance et d’un certain développement dans le secteur des TIC au cours des
deux dernières décennies, bien qu’il soit encore loin derrière plusieurs pays
africains.

En 2018, le nombre total d’abonné·e·s aux services de télécommunications a
atteint 43,6 millions, ce qui représente une augmentation de 15% par rapport à
l’année précédente. Le pays compte 41,92 millions d’abonné·e·s à la téléphonie
mobile et 22,3 millions d’utilisatrices et d’utilisateurs de données et
d’internet. Après des années de faible adoption en raison de la tarification
élevée, Ethio telecom a réduit ses tarifs de 40% à 50% en 2018, ce qui a
entraîné une augmentation de l’utilisation des données et du trafic vocal. Cette
augmentation s’est faite à un rythme spectaculaire : 130% pour l’utilisation des
données et 19% pour la voix[^9].

Les efforts de l’Éthiopie en matière d’atténuation, d’adaptation et de
résilience au dérèglement climatique sont ancrés dans sa stratégie CRGE, publiée
en 2011. Sa mise en œuvre est actuellement supervisée et coordonnée par le
ministère de l’environnement et du dérèglement climatique[^10]. Cette stratégie,
ainsi que sa CDN issue de l’accord de Paris, est en cours de révision. En juin
2020, l’Éthiopie a dévoilé son tout premier plan de développement économique
décennal intitulé « Éthiopie : un phare africain de prospérité ». Selon le
premier ministre, le plan décennal vise à apporter une croissance économique
basée sur la qualité, à augmenter la production et la compétitivité, à
construire une économie verte et résiliente au dérèglement climatique, à
réaliser une transformation institutionnelle, à assurer des opportunités justes
et équitables pour les femmes et les jeunes, et à garantir une croissance tirée
par le secteur privé[^11].

Pour aller plus loin, l’Éthiopie a également lancé récemment sa stratégie
« Digital Ethiopia 2025 : une stratégie numérique pour la prospérité inclusive
de l’Éthiopie » pour les cinq prochaines années. Cette stratégie s’appuie sur
son programme de réforme économique national visant à réaliser la transformation
numérique du pays. Cela se fera par la création d’une société numériquement
active, axée sur quatre secteurs stratégiques, à savoir l’agriculture,
l’industrie manufacturière, le développement de services fondés sur les
technologies de l’information et le tourisme [^12]. La transformation numérique
sera donc au cœur de la réalisation du programme de réforme économique des cinq
prochaines années.

## Technologies climatiques en Éthiopie

Les TIC offrent des solutions pour surveiller, atténuer et s’adapter aux défis
du dérèglement climatique. Plusieurs initiatives en matière de technologies
climatiques dans le pays démontrent l’éventail des mesures déployées par les
différentes parties prenantes pour réduire les émissions de gaz à effet de
serre, renforcer la résilience face à la crise climatique et contribuer aux
objectifs du pays pour 2030.

Pour les secteurs agricole et agro-pastoral, un changement de climat signifie
que les techniques et les connaissances traditionnelles peuvent ne plus
fonctionner. La modification des phénomènes météorologiques pouvant entraîner
des changements dans l’environnement exige de nouvelles solutions et une
meilleure information des agricultrices et agriculteurs. À cet égard, les TIC
jouent un rôle important grâce à l’utilisation d’applications et de services
mobiles, notamment les SMS et la réponse vocale interactive (RVI). À cette fin,
l’Éthiopie a lancé une plateforme numérique de conseil en agroclimatologie,
appelée EDACaP, qui se compose de quatre éléments complémentaires : un hub de
base de données agroclimatiques, la modélisation du climat, la modélisation des
cultures et une plateforme de diffusion. Les données sont ensuite interprétées,
et des conseils sont produits sur les prévisions de rendement, l’agroclimat et
les scénarios climatiques qui ont des éléments ciblés, y compris la sélection
des champs de culture et des variétés, le moment de la plantation et de la
récolte, les approches d’irrigation idéales, ainsi que des mesures pour prévenir
les ravageurs et les maladies. Les conseils sont diffusés par SMS, RVI et radio
aux agent·e·s de développement et aux agricultrices et agriculteurs dans les
langues locales. Cela aide les petit·e·s exploitant·e·s agricoles à gérer les
risques climatiques, améliore leur capacité d’adaptation et renforce leur
résilience.

La phase pilote d’EDACaP a déjà aidé 82 000 petit·e·s exploitant·e·s agricoles
dans tout le pays et devrait atteindre 16,7 millions de fermières et fermiers
une fois qu’elle aura été étendue par le ministère de l’agriculture. Il s’agit
notamment d’atteindre les plus de 60 000 agent·e·s agricoles du pays dans des
zones géographiques et des chaînes de valeur agricoles spécifiques. La
traduction de cette science complexe sous une forme compréhensible par les
petit·e·s exploitant·e·s agricoles permettrait d’améliorer leur prise de
décision dans divers domaines, de la production à l’accès au marché pour leurs
produits[^13]. La plateforme EDACaP devrait atteindre 86 districts (woredas)
ciblés dans le cadre du programme de croissance agricole (AGP), couvrant huit
états régionaux, et 25 centres de recherche agricole (17 fédéraux et huit
régionaux)[^14].

Un service similaire appelé YeZaRe[^15], développé par l’entreprise sociale
Echnoserve, est un système numérique mobile qui fournit des données
météorologiques et de marché aux petit·e·s exploitant·e·s agricoles, et qui
relie ces derniers aux marchés afin d’augmenter leurs revenus et leur résistance
au dérèglement climatique. Le service compte actuellement plus de 35 000
utilisatrices et utilisateurs enregistré·e·s, dont des agricultrices et
agriculteurs, des coopératives, des agent·e·s de vulgarisation et des
grossistes[^16]. Comme l’indique la stratégie sectorielle, les secteurs de
l’agriculture et de la sylviculture sont les plus vulnérables aux effets du
dérèglement climatique. Ils jouent également un rôle majeur dans l’économie
éthiopienne, étant donné qu’ils contribuent à 43% du PIB du pays, à environ 80%
de l’emploi et à environ 75% de la valeur des produits d’exportation[^17].

Dans ce contexte, renforçant les initiatives précédentes, une méga-initiative de
reforestation menée par le premier ministre dans le but de planter 20 milliards
d’arbres sur une période de quatre ans a débuté en 2019 avec plus de quatre
milliards de semis plantés. Cela comprenait près de 350 millions plantés en une
période de 12 heures en juillet 2019,[^18] ce qui serait un record mondial. En
août 2020, le gouvernement a annoncé qu’il avait atteint l’objectif de planter
cinq milliards d’arbres[^19].

Lors de la plantation et de l’entretien futur des arbres, le ministère de
l’innovation et de la technologie et l’Institut éthiopien des sciences et
technologies spatiales ont été chargés d’assurer la durabilité des arbres
plantés à l’aide d’un système numérique. Les deux institutions ont utilisé les
coordonnées GPS pour cartographier les sites de plantation. Cette initiative
devait permettre d’augmenter de manière significative la couverture forestière
nationale de 15,5% en 2019 à 20% en 2020 [^20].

Le récent rapport sur la CDN de l’Éthiopie pour le secteur des transports montre
que sa contribution pourrait atteindre 12,2 Mt eqCO₂ en 2030 par rapport au
statu quo, ce qui est encore légèrement supérieur à la contribution de réduction
des émissions visée pour le secteur des transports dans la CDN de l’Éthiopie
(10 Mt eqCO₂). Cela signifie que des mesures d’atténuation supplémentaires sont
nécessaires.

On estime qu’environ 90% du transport lié à l’import-export ainsi que 95% des
services de transport public sont assurés par le transport routier, qui était
responsable de 5,5 Mt eqCO₂ d’émissions en 2013[^21]. Le nombre total de
véhicules immatriculés en Éthiopie actuellement, y compris les vélomoteurs et
les véhicules assemblés localement, est de 1 071 345, avec plus de la moitié du
total immatriculé situé dans la capitale Addis-Abeba (55,63%). Viennent ensuite
les régions d’Oromia (15,98%), d’Amhara (8,7%), du Sud (8,7%), du Tigré (4,9%),
de Dire Dawa (2,04%), de Somali (1,4%), de Benishangul (0,82%), de Harar
(0,74%), d’Afar (0,61%) et de Gambella (0,51%)[^22]. Le secteur étant
principalement alimenté par des combustibles fossiles, il est responsable
d’externalités environnementales telles que les émissions de gaz à effet de
serre.

À cet égard, parmi les possibilités d’atténuation identifiées par les stratégies
nationales figurent l’amélioration des transports en commun grâce à un système
ferré léger, un système de transport rapide par bus utilisant des trolleybus
électroniques, des normes de rendement énergétique pour les véhicules, ainsi que
des carburants et des systèmes de propulsion alternatifs, comme l’augmentation
de la part des véhicules hybrides et électriques rechargeables.

À cette fin, de petits projets pilotes de voitures électriques à batterie ont
été mis en place. Entre autres, une initiative de voitures électriques a été
lancée par la société japonaise de fabrication d’automobiles électriques,
Mitsui, qui a testé avec succès sa voiture électrique à trois roues (E-Trike) et
a lancé une usine d’assemblage en Éthiopie en avril 2019. E-Trike consomme 7,3
kilowatts d’énergie électrique par heure et peut parcourir 80 kilomètres après
avoir rechargé complètement sa batterie pendant six à sept heures[^23] Avant
cette initiative, avec le soutien du Programme des Nations unies pour le
développement (PNUD), un projet pilote de véhicule électronique a été mis en
œuvre par l’Autorité de protection de l’environnement et dVentus Technologies en
2013, lançant 12 e-taxis pour desservir quatre villes d’Éthiopie. Conçu pour
utiliser des batteries, cet e-taxi peut couvrir plus de 50 kilomètres une fois
entièrement chargé[^24]. Ces deux véhicules ont été introduits en soutien à la
politique économique verte éthiopienne pour démontrer l’énorme potentiel qu’ils
ont pour réduire la pollution environnementale.

Outre ces initiatives pilotes, une usine complète d’assemblage de voitures
entièrement électriques a été créée par Marathon Motor Engineering, une
coentreprise entre le grand coureur de fond Haile Gebreselassie et Hyundai. La
voiture électrique Hyundai Ioniq est un véhicule à hayon compact avec un trio de
groupes motopropulseurs qui peut parcourir jusqu’à 200 kilomètres avec une seule
charge[^25]. Des études montrent que conduire la Hyundai Ioniq serait six fois
moins cher que le coût du même trajet dans une Toyota Corolla[^26].

Enfin, l’un des domaines dans lesquels les technologies climatiques ont un
impact important est le secteur de l’énergie, qui représente environ 15% des
émissions totales de l’Éthiopie, soit 22 Mt eqCO₂.[^27] La stratégie du pays en
matière de réduction des émissions pour atteindre son objectif d’atténuation
pour 2030 dans ce domaine comprend le passage à des technologies modernes et
efficaces sur le plan énergétique.

Un exemple qui présente un grand potentiel pour assurer l’efficacité énergétique
est l’utilisation de compteurs électriques intelligents. À cet égard, dVentus
Technologies propose un système de réseau intelligent qui peut être adapté à
différents environnements d’exploitation [^28]. Ce système améliore l’efficacité
énergétique, en augmentant l’accès à l’énergie et en réduisant les déchets. Le
système est également disponible en monophasé et triphasé, permettant une
communication bidirectionnelle par Wi-Fi, WLAN ou GSM/GPRS, ce qui permet la
connexion ou la déconnexion à distance. dVentus Technologies a également obtenu
une subvention dans le cadre du programme du Fonds pour l’énergie durable en
Afrique de la Banque africaine de développement pour établir une usine locale de
fabrication de compteurs intelligents en Éthiopie[^29].

Chacune de ces initiatives soutiendra un ou plusieurs des quatre piliers de
l’économie verte mentionnés ci-dessus, et complétera les programmes et mesures
politiques existants visant à accroître l’efficacité des ressources. Toutefois,
pour que le pays atteigne ses objectifs 2030 en matière d’économie verte,
d’autres améliorations sont nécessaires, notamment une réduction de l’intensité
des émissions (dans le secteur industriel compte tenu de l’ambition de
l’Éthiopie de devenir la plaque tournante de l’industrie manufacturière en
Afrique) et de la part des combustibles fossiles (dans le bouquet énergétique
national), ainsi qu’un renforcement de l’agriculture climato-consciente face au
climat.

## Conclusion

Le rapport souligne comment les TIC peuvent être exploitées pour atténuer les
effets du dérèglement climatique, s’y adapter et les surveiller. Cela inclut les
différents domaines d’application de la surveillance du climat et des prévisions
météorologiques, qui sont essentiels pour les communications et les avis
d’alerte précoce et de secours en cas de catastrophe, comme le montre la mise en
œuvre d’EDACaP. En outre, les TIC et d’autres technologies sous la forme de
réseaux électriques intelligents ont démontré un grand potentiel dans la lutte
contre le dérèglement climatique en aidant à distribuer et à utiliser l’énergie
plus efficacement, et à intégrer des sources d’énergie renouvelables dans leurs
systèmes. Par exemple, les compteurs électriques intelligents de dVentus
Technologies ont un impact direct sur l’efficacité de la facturation et de la
gestion de la charge, ce qui entraine moins de pertes d’énergie et de pannes de
courant.

Le programme agressif de reboisement actuellement en cours en Éthiopie est très
prometteur pour la réalisation des émissions négatives possibles de −40 Mt eqCO₂
dans la sylviculture[^30] qui pourraient permettre d’atteindre globalement la
contribution déterminée au niveau national prévue d’ici 2030. D’autre part, le
secteur des transports devrait avoir augmenté sa contribution aux émissions de
10 Mt eqCO₂ à 30 Mt eqCO₂. Dans ce contexte, certaines des initiatives
concernant les normes d’efficacité et l’adoption accrue de véhicules électriques
pourraient jouer un rôle important dans la limitation des émissions. Cela est
d’autant plus vrai que diverses mesures d’incitation et réglementations
prévoient l’adoption accrue de moyens de transport respectueux de
l’environnement au cours de la prochaine décennie afin d’atteindre les objectifs
fixés.

Enfin, d’une part, la prochaine stratégie numérique quinquennale du gouvernement
pour l’Éthiopie devrait permettre une numérisation accrue et l’adoption de
technologies numériques pour aider à atténuer le dérèglement climatique. D’autre
part, l’ambition de l’Éthiopie de devenir un centre manufacturier en Afrique
doit être alignée sur la promotion des technologies vertes et la réalisation de
la transformation numérique des processus industriels en adoptant
progressivement les technologies de l’industrie 4.0 qui répondent au défi du
dérèglement climatique.

## Mesures à prendre

Bien que les pays soient confrontés à une trajectoire incertaine de leurs
économies quand il s’agit de développement à long terme, il apparait que la
pandémie de COVID-19 catalysera la transformation numérique et l’adoption de la
technologie. La pandémie est également un moment opportun pour aligner les
objectifs de développement sur les objectifs du dérèglement climatique. Dans ce
contexte, les mesures suivantes sont recommandées aux décideuses et décideurs
politiques et aux autres parties prenantes :

- Améliorer l’accès aux infrastructures, notamment les interconnexions de
  l'internet vers le réseau mondial et les réseaux nationaux et interrégionaux.

- Créer des cadres politiques et des incitations favorables, notamment en
  s’appuyant sur la réforme actuelle du marché des télécommunications qui permet
  l’arrivée de nouveaux entrants.

- Investir dans les compétences et la formation afin de maximiser les avantages
  des infrastructures numériques. Il s’agit notamment d’investir dans
  l’éducation par la formation continue et de promouvoir les compétences
  numériques et autres nouvelles technologies.

- Créer des partenariats entre les secteurs public et privé pour faciliter les
  échanges entre les décideuses et décideurs et celles et ceux qui fournissent
  les services numériques par le biais de pôles d’innovation et d’incubateurs
  désignés afin de promouvoir l’innovation dans les technologies climatiques.

[^1]: <https://data.worldbank.org/indicator/SP.POP.TOTL?locations=ET>

[^2]: République démocratique fédérale d’Éthiopie. (2011). *Ethiopia's Climate-Resilient Green Economy Strategy*.
    <https://www.adaptation-undp.org/sites/default/files/downloads/ethiopia_climate_resilient_green_economy_strategy.pdf>

[^3]: Équivalent carbone, une façon d’exprimer tous les différents gaz à effet de serre en un chiffre unique.

[^4]: Wang-Helmreich, H., & Mersmann, F. (2018). *Implementation of Nationally Determined Contributions: Ethiopia Country Report*. Umweltbundesamt.
    <https://www.umweltbundesamt.de/en/publikationen/implementation-of-nationally-determined-4>

[^5]: Ibid.

[^6]: République démocratique fédérale d’Éthiopie. (2011). Op. cit.

[^7]: Ibid.

[^8]: République démocratique fédérale d’Éthiopie. (2016). *The National Information and Communication Technology (ICT) Policy and Strategy: Final Draft.*
    <https://mint.gov.et/docs/the-national-information-and-communication-technology-ict-policy-and-strategy-2/?lang=en>

[^9]: Ethio telecom. (2019). *Ethio telecom 2018/2019 Ethiopian Fiscal Year Business Performance Report*.
    <https://www.ethiotelecom.et/2018-19-efy-p-reporte/>

[^10]: République démocratique fédérale d’Éthiopie (2016). *Growth and Transformation Plan II (GTP II) (2015/16-2019/20). Volume I: Main
    Text*. <https://ethiopia.un.org/en/download/2447/15231>

[^11]: Ethiopian Monitor. (11 juin 2020). Ethiopia Unveils 10-Year Development Plan.
    [https://ethiopianmonitor.com/2020/06/11/ethiopia-unveils-10-year-development-plan](https://ethiopianmonitor.com/2020/06/11/ethiopia-unveils-10-year-development-plan/)

[^12]: Ministère de l'innovation et de la technologie. (2020). *Digital Ethiopia 2025: A Digital Strategy for Ethiopia Inclusive Prosperity*.
    <https://www.pmo.gov.et/media/other/b2329861-f9d7-4c4b-9f05-d5bc2c8b33b6.pdf>

[^13]: Samuel, S. (16 novembre 2019). Launching digital agro-climate advisory platform in Ethiopia. *The Reporter*.
    <https://www.thereporterethiopia.com/article/launching-digital-agro-climate-advisory-platform-ethiopia>

[^14]: Ibid.

[^15]: <https://yezare.info/index.php>

[^16]: <https://yezare.info/PublicMarket.php>

[^17]: République démocratique fédérale d’Éthiopie. (2015). *Ethiopia's Climate Resilient Green Economy -- Climate Resilience Strategy: Agriculture and Forestry*.

[^18]: Myers, J. (5 juin 2020). Ethiopia wants to plant 5 billion seedlings this year. *World Economic Forum*.
    [https://www.weforum.org/agenda/2020/06/ethiopia-is-going-to-plant-5-billion-seedlings-this-year](https://www.weforum.org/agenda/2020/06/ethiopia-is-going-to-plant-5-billion-seedlings-this-year/)

[^19]: FBC. (2020). Ethiopia Successfully Finalises Planting of 5 billion trees: PM Abiy Ahmed. *FBC*.
    <https://www.fanabc.com/english/ethiopia-successfully-finalizes-planting-of-5-billion-trees-pm-abiy-ahmed>

[^20]: Hailemariam, B. (27 juillet 2019). Tree Planting Campaign: Audacity or a Pipe Dream? *Addis Fortune*.
    [https://addisfortune.news/tree-planting-campaign-audacity-or-a-pipe-dream](https://addisfortune.news/tree-planting-campaign-audacity-or-a-pipe-dream/)

[^21]: Wang-Helmreich, H., & Mersmann, F. (2018). Op. cit.

[^22]: Le nombre total de véhicules que le pays a importés et enregistrés au cours de l'exercice 2018–2019 (qui s'est terminé le 7 juillet 2019) a dépassé de 30 834 le montant que l'Éthiopie avait importé l'année précédente, pour devenir 135 457 véhicules en un an. New Business Ethiopia. (3 août 2019). L'Éthiopie importe 135 457 véhicules en un an.
[https://newbusinessethiopia.com/trade/ethiopia-imports-135-457-vehicles-in-a-year](https://newbusinessethiopia.com/trade/ethiopia-imports-135-457-vehicles-in-a-year/)

[^23]: Behailu, M. (27 avril 2019). Japanese company introduces electric car in Ethiopia. *The Ethiopian Herald*.
    [https://www.press.et/english/?p=5145\#](https://www.press.et/english/?p=5145)

[^24]: PNUD. (29 mars 2013). Ethiopia Pilots Electric Vehicles.
    <https://www.et.undp.org/content/ethiopia/en/home/presscenter/articles/2013/03/29/ethiopia-pilots-electric-vehicles-.html>

[^25]: Tekle, T. (28 juillet 2020). Ethiopia unveils locally-assembled electric car. *The East African*.
    <https://www.theeastafrican.co.ke/tea/business/ethiopia-unveils-locally-assembled-electric-car-1907430>

[^26]: Kuhudzai, R. J. (27 juillet 2020). First Ethiopian-Assembled All-Electric Hyundai Ioniq Rolls Out of Haile Gebreselassie's Marathon Motor Engineering Plant. *CleanTechnica*.
   [https://cleantechnica.com/2020/07/27/first-ethiopian-assembled-all-electric-hyundai-ionic-rolls-out-of-haile-gebrselassies-marathon-motor-engineering-plant](https://cleantechnica.com/2020/07/27/first-ethiopian-assembled-all-electric-hyundai-ionic-rolls-out-of-haile-gebrselassies-marathon-motor-engineering-plant/)

[^27]: Wang-Helmreich, H., & Mersmann, F. (2018). Op. cit.

[^28]: <http://www.dventus.com/SmartElectricMeter.html>

[^29]: Banque africaine de développement. (16 décembre 2014). SEFA to support dVentus Technologies in the manufacturing of smart meters in Ethiopia.
    <https://www.afdb.org/en/news-and-events/sefa-to-support-dventus-technologies-in-the-manufacturing-of-smart-meters-in-ethiopia-13864>

[^30]: Wang-Helmreich, H., & Mersmann, F. (2018). Op. cit.
