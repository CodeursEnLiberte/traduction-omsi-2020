[]{#anchor}Rapport thématique

Autrice : Paz Peña[^1]

URL: [pazpena.com]

# Titre : Plus, mieux, plus grand, plus vite : le paradoxe écologique des économies numériques

L’avancée du pouvoir technologique et la réduction de ses coûts de manufacture
ont créé un écosystème de technologies numériques indépendantes qui soutiennent
la transformation numérique. D’après l’Organisation de coopération et de
développement économiques (OCDE)[^2], cet écosystème va évoluer et continuer à
conduire le changement économique et social futur. L’écosystème est actuellement
soutenu par l’internet of things [l’internet des objets] (IoT), les réseaux sans
fil nouvelle génération (5G), le cloud, l’analyse big data, l’intelligence
artificielle, la blockchain et le calcul haute-performance – même s’il est aussi
probable que les technologies qui constituent l’évolution de l’écosystème
changeront au cours du temps.

Devant nous, disent-ils, se trouve une révolution. Cependant, il est tout aussi
facile d’argumenter que cela semble plutôt une nouvelle évolution de la même
chose : le capitalisme a trouvé une nouvelle vie avec les technologies
numériques. Dans la continuité des pratiques extractivistes et colonialistes,
cette fois-ci c’est l’expérience humaine qui est briguée comme matière première
pour transiter vers des données de comportement.[^3]

La nouvelle « révolution » est appelée la Quatrième révolution industrielle[^4]
et pour les entreprises auxquelles elle bénéficie, cela sonne comme une joyeuse
révolte. Désormais les entreprises peuvent exploiter chaque détail de notre
quotidien sans même dépendre du fait que nos appareils soient allumés ou
éteints : les « villes intelligentes » et tous nos comportements suivis par des
« appareils intelligents » (IoT) peuvent être compilés et traités par plusieurs
entreprises et vendus sur les marchés à terme de l'analyse comportementale qui
s'étendent, au-delà de la publicité ciblée, à de nombreux autres secteurs.[^5]

Mais les révolutions nécessitent de la vitesse. Un sentiment d’urgence infecte
les États léthargiques qui manquent d’idées pour généraliser le bien-être
social. Les initiatives en politique publique sont maintenant dictées par un
secteur privé qui n'hésite pas à réclamer une bouffée d'air aux gouvernements
en leur demandant de faciliter la « transformation numérique ». C’est une situation
gagnant-gagnant : les entreprises privées auront des mines de données infinies
(chacune et chacun d’entre nous) et les États pourront avoir une augmentation de
la production et, par conséquent, de meilleurs chiffres de croissance.

# Le changement climatique comme une occasion de faire des affaires

La transformation numérique a reçu un coup de pouce inattendu et spectaculaire
il y a tout juste cinq ans. Le 12 décembre 2015, à la Conférence des Nations
unies sur les changements climatiques à Paris (COP21), les membres de la
Convention-cadre des Nations unies sur les changements climatiques (CCNUCC) ont
abouti à un accord historique pour combattre l’urgence climatique et accélérer
et intensifier les actions et investissements nécessaires pour un futur durable
et sobre en carbone. L’atténuation du changement climatique signifie que la
consommation d’énergie doit être réduite – principalement par la mise en place
d’un système d’électricité renouvelable.[^6]

L’accord de Paris mentionne explicitement l’innovation dans l’article 10,
paragraphe 5. En outre, pour exploiter pleinement le potentiel des technologies
climatiques, le CCNUCC établit qu’il est crucial d’innover et d’utiliser des
« technologies révolutionnaires » dans d’autres domaines pour améliorer nos vies
« comme les nano-technologies, la blockchain, l’internet des objets et d’autres
technologies de l’information et de la communication ».[^7] Le CCNUCC nous
rappelle aussi que l’innovation technologique doit être inclusive et équitable
pour un avoir un impact maximal.

D’après Rieger,[^8] en théorie, les technologies de l’information et de la
communication (TIC) peuvent mener à la dématérialisation (entendue comme la
baisse de l’utilisation des ressources) de trois manières. D’une part, en
substituant le virtuel aux biens matériels, par exemple, en remplaçant les
exemplaires physiques des albums de musique par des copies numériques. D’autre
part, le secteur des TIC a un impact environnemental plus faible que beaucoup
d’autres domaines. En fonction du type de secteur économique qu’elle remplace,
sa croissance pourrait réduire globalement les émissions totales pour
l’économie. En effet, la durabilité a été identifiée comme l’un des principaux
bénéfices de l’économie numérique, particulièrement au niveau des processus de
fabrication pour lesquels l’allocation de ressources (produits, matériaux,
énergie et eau) peut être faite plus efficacement par une gestion intelligente
en utilisant des technologies variées.[^9]

Enfin, la large utilisation de ces technologies augmenterait l’efficacité
énergétique et des ressources. De plus, d’après, le Global e-Sustainability
Initiative (GeSI) [Initiative mondiale de cyberdurabilité], dans un rapport
préparé par l’entreprise privée Accenture, les TIC peuvent permettre une
réduction de 20% des émissions globales de CO₂ d’ici 2030, les maintenant au
niveau de 2015 : « Cela signifie que nous pouvons potentiellement éviter de
choisir entre prospérité économique et protection environnementale. »[^10]

# Le paradoxe écologique de l’économie numérique

Cependant, il est vital de comprendre que les effets bénéfiques des TIC – réduire
la consommation d’énergie et faciliter la transition vers les énergies
renouvelables – doit être mesurée relativement aux effets néfastes directs du
changement vers une économie numérique. Ces effets comprennent les émissions
dues à l’augmentation de la production, de l’utilisation et des déchets des
TIC.[^11] Autrement dit, nous devons prendre en compte les coûts matériels de
l’imaginaire éthéré de la numérisation.

Il est reconnu que l’évolution de l’écosystème technologique soutenant
l’économie numérique s’accompagne d’une hausse prodigieuse de la consommation
énergétique ;[^12] cependant, cette corrélation positive entre numérisation et
consommation d’énergie ne tient pas pour tous les pays et tous les vecteurs
énergétiques.[^13] Pour répondre à ces défis fondamentaux sur les systèmes et
appareils de télécommunication, une vision holistique appelée « communications
vertes » a évolué, et cherche à améliorer l’efficacité énergétique à grande
échelle des communications et des réseaux informatiques.[^14] Par exemple, des
efforts sont faits pour réduire la consommation d’énergie liée au déploiement de
la 5G et aux centres de données, entre autres technologies.[^15]

Alors que l’efficacité énergétique a augmenté dans le secteur des TIC depuis des
décennies, les promesses de réduction de consommation d’énergie par la
numérisation n’ont jusqu’ici pas été tenues. D’après une étude récente par Lange
et al., « la numérisation détruit ainsi son propre potentiel » de réduction de
la demande en énergie.[^16]

Ajouté à cela, comme le montrent de récentes découvertes concernant la
dématérialisation et les TIC en Europe :

> S’il est probable que la dématérialisation ait eu lieu dans des secteurs bien
> précis de l’économie – la numérisation de la musique, des livres et des films
> sont des exemples, ainsi que la popularisation des télécommunications, des
> visioconférences et l’omniprésence du e-commerce – c’est un changement encore
> limité qui n’a pas eu d’impact global sur la consommation.[^17]

Ce paradoxe produit par l’augmentation de la production, de l’utilisation et des
déchets des TIC impacte aussi directement la gestion des déchets d'équipements
électriques et électroniques (D3E), ou déchets électroniques. La
miniaturisation, l’obsolescence des appareils, et la polyvalence accrue des
appareils (par exemple, avec la nouvelle génération d’appareils compatibles avec
la 5G) ont contribué à la redondance des appareils plus vieux.[^18] D’après
Forti et al.,[^19] en moyenne, le poids total de la consommation globale des
équipements électriques et électroniques augmente chaque année de 2,5 million de
tonnes, même sans compter les panneaux photovoltaïques. En outre, en 2019, un
total saisissant de 53,6 millions de tonnes de déchets électroniques ont été
générés dans le monde, soit 7,3 kg par personne en moyenne.

Une valeur estimée à 57 milliards de dollars de matières premières secondaires
était présente (au total) dans les D3E générés en 2019.[^20] L’exploitation de
mines urbaines essaye de récupérer les matériaux secondaires et de réduire
l’épuisement des matières premières primaires. Néanmoins, ceci n’est pas
toujours viable, essentiellement parce que cela engendre une pollution de l’air,
de l’eau, et du sol due aux effluents émanant d’activités de recyclage souvent
officieuses. De plus, la conception de dispositifs facilitant leur recyclage
futur est toujours un défi.[^21]

Les coûts écologiques de l’extraction de matières premières pour manufacturer la
nouvelle génération d’appareils technologiques, y compris les technologies
vertes, doivent aussi être gardés à l’esprit. Les conflits politiques,
environnementaux et culturels créés par « l’extractivisme vert », qui ne fait
que creuser le fossé économique entre pays développés et non-développés,
devraient être un sérieux indicateur des coûts réels de l’innovation, et, de
façon encore plus importante, de qui finit par en payer le prix.[^22]


Les humains font aussi partie du paradoxe écologique dans cette chaine
extractiviste. Plus les technologies sont efficaces, plus les humains seront
exploités comme matière première, puisque nous sommes les sources des profits du
capitalisme de surveillance.[^23] Les coûts matériels de la numérisation vont
au-delà de l’utilisation des ressources naturelles ; elles comprennent aussi
l’extractivisme humain. Cependant, les conséquences de cela sur l’environnement
sont encore à examiner. Pour l’instant, on peut affirmer que, faisant partie du
cycle du capitalisme, l’exploitation de nos données est partiellement motivée
par l’encouragement à la consommation infinie dans les économies numériques.

# Technologie pour une transformation socio-écologique égalitaire

Conformément aux concepts hégémoniques de l’économie numérique, l’urgence
climatique est plus une opportunité commerciale qu’une crise sans précédent
produite par le Capitalocène. Cela s’est traduit par la domination d’une vision
néolibérale dépolitisée sur les technologies actuelles. Leur conception et leur
déploiement cherche à résoudre des problèmes de durabilité structurels par
l’efficacité et la productivité pures, en les alignant avec des politiques
d’austérité.[^24] La logique de pur extractivisme appliquée aux technologies est
en contradiction avec tout standard éthique post-humain[^25] et ouvre la voie à
des atrocités comme « l’apartheid climatique ».[^26]

En ces temps d’urgence du Capitalocène, il est impératif de créer des
alternatives technologiques ; mais plutôt que de concevoir des hackerspace ou
des entreprises open-source, tentatives louables mais individuelles et fragiles
en l’absence d’horizon politique, le défi consiste à déployer les technologies
numériques dans une configuration socio-économique et socio-environnementale
qualitativement différente et qui ne soit pas simplement « moins de la même
chose ».[^27] Dans ce contexte, il est peut-être temps d’examiner d’un œil
critique le projet de décroissance.

La décroissance est un projet de transformation socio-écologique radical et
égalitaire qui vise à décoloniser l’imaginaire social de la poursuite de la
croissance infinie.[^28] Comme l’affirment Mastini et al., la décroissance
cherche une réduction d’échelle équitable du flux de production avec la garantie
conséquente du bien-être.[^29] Son hypothèse est que la qualité de vie peut
s’améliorer alors que le PIB diminue. De ce point de vue, le capitalisme et son
paradigme de croissance économique nous ont conduit à une limite planétaire où
il n’est pas faisable de réduire les émissions de carbone suffisamment vite. Et
puis, en s’appuyant sur l’Histoire, la décroissance rejette l’idée que le seul
déploiement d’énergies renouvelables peut suffire à supplanter les combustibles
fossiles dans la production d’énergie, étant donné que, par exemple, la
découverte du pétrole comme source d’énergie n’a pas remplacé le charbon.

Le paradigme de la décroissance est encore naissant, et il reste beaucoup à
faire, y compris le rôle critique que les technologies doivent y jouer.[^30]
Pour le reste, la transition vers la décroissance doit être planifiée comme un
effort planétaire et participatif pour éviter des inégalités structurelles.[^31]
Avec tous ses défis infinis, la décroissance peut être un stimulus concret pour
les technologistes, la société civile, le monde universitaire, les gouvernements
et les entreprises pour s’éloigner d’une logique extractiviste et dessiner une
économie numérique durable.

L’humanité n’a pas de temps à perdre. Si nous voulons survivre en tant
qu’espèce, nous avons besoin d’innovation structurelle. Nous devons avancer dans
une autre direction, où les humains et non-humains, y compris les machines
intelligentes, peuvent coexister solidairement face aux défis d’une planète qui,
que cela nous plaise ou non, est déjà irrémédiablement différente.

[^1]: Paz Peña est une consultante indépendante et une activiste à
l'intersection de la technologie, du féminisme et de la justice
sociale. Contact : paz\@pazpena.com

[^2]: OECD. (2019). *Going Digital: Shaping Policies, Improving Lives*.
OECD Publishing. <https://doi.org/10.1787/9789264312012-en>

[^3]: Couldry, N., & Mejias, U. (2019). Data colonialism: rethinking big
data's relation to the contemporary subject. *Television and New
Media, 20*(4), 336-349; Zuboff, S. (2019). *The Age of Surveillance
Capitalism: The Fight for a Human Future at the New Frontier of
Power*. Profile Books.

[^4]: Schwab, K. (2016, 14 January). The Fourth Industrial Revolution:
what it means, how to respond. *World Economic Forum*.
[https://www.weforum.org/agenda/2016/01/the-fourth-industrial-revolution-what-it-means-and-how-to-respond](https://www.weforum.org/agenda/2016/01/the-fourth-industrial-revolution-what-it-means-and-how-to-respond/)

[^5]: Zuboff, S. (2019). Op. cit.

[^6]: UNFCCC. (2017). Technological Innovation for the Paris Agreement:
Implementing nationally determined contributions, national
adaptation plans and mid-century strategies.
<https://unfccc.int/ttclear/tec/brief10.html>; Lange, S., Pohl, J.,
& Santarius, T. (2020). Digitalization and energy consumption. Does
ICT reduce energy demand? *Ecological Economics*, *176*.
<https://doi.org/10.1016/j.ecolecon.2020.106760>

[^7]: UNFCCC. (2017). Op. cit.

[^8]: Rieger, A. (2020). Does ICT result in dematerialization? The case
of Europe, 2005-2017. *Environmental Sociology, 7*(1), 64-75.
<https://doi.org/10.1080/23251042.2020.1824289>

[^9]: Stock, T., & Seliger, G. (2016). Opportunities of Sustainable
Manufacturing in Industry 4.0. *Procedia CIRP, 40*, 536-541.
<https://doi.org/10.1016/j.procir.2016.01.129>.

[^10]: GeSI. (2015). *\#SMARTer2030: ICT Solutions for 21st Century
Challenges*.
<https://smarter2030.gesi.org/downloads/Full_report.pdf>

[^11]: Lange, S., Pohl, J. & Santarius, T. (2020). Op. Cit.

[^12]: World Economic Forum. (2016). *Digital Transformation of
Industries: Societal Implications*.
<https://reports.weforum.org/digital-transformation/wp-content/blogs.dir/94/mp/files/pages/files/dti-societal-implications-white-paper.pdf>;
Gandotra, P., & Jha, R. K. (2017). A survey on green communication
and security challenges in 5G wireless communication networks.
*Journal of Network and Computer Applications, 96*, 39-61.
<https://doi.org/10.1016/j.jnca.2017.07.002>

[^13]: Lange, S., Pohl, J., & Santarius, T. (2020). Op. cit.

[^14]: Wu, J., Rangan, S., & Zhang, H. (2016). *Green Communications:
Theoretical Fundamentals, Algorithms, and Applications*. CRC Press.

[^15]: Cho, R. (2020, 13 August). The Coming 5G Revolution: How Will It
Affect the Environment? *Earth Institute*.
[https://blogs.ei.columbia.edu/2020/08/13/coming-5g-revolution-will-affect-environment](https://blogs.ei.columbia.edu/2020/08/13/coming-5g-revolution-will-affect-environment/)

[^16]: Lange, S., Pohl, J., & Santarius, T. (2020). Op. cit.

[^17]: Rieger, A. (2020). Op. cit.

[^18]: Shittu, O. S., Williams, I. D., & Shaw, P. J. (2021). Global
E-waste management: Can WEEE make a difference? A review of e-waste
trends, legislation, contemporary issues and future challenges.
*Waste Management, 120*, 549-563.
<https://doi.org/10.1016/j.wasman.2020.10.016>

[^19]: Forti, V., Baldé, C. P., Kuehr, R., & Bel, G. (2020). *Suivi des
D3E à l'échelle mondiale pour 2020 : Quantités, flux et possibilités
offertes par l'économie circulaire*. Université des Nations unies (UNU)/
Institut des Nations unies pour la formation et la recherche (UNITAR) --
Programme cohébergé SCYCLE, Union internationale des télécommmunications
(UIT) & International Solid Waste Association (ISWA).
<https://www.itu.int/pub/D-GEN-E_WASTE.01-2020/fr>

[^20]: Ibid.

[^21]: Shittu, O. S., Williams, I. D., & Shaw, P. J. (2021). Op. cit.

[^22]: Fuchs, R., Brown, C., & Rounsevell, M. (2020). Europe's Green
Deal offshores environmental damage to other nations. *Nature, 586*,
671-673. https://doi.org/10.1038/d41586-020-02991-1; Riofrancos, T.
(2020, 28 September) Field Notes from Extractive Frontiers. *Center
for Humans & Nature*.
<https://www.humansandnature.org/field-notes-from-extractive-frontiers>

[^23]: Zuboff, S. (2019). Op. cit.

[^24]: March, H. (2018). The Smart City and other ICT-led
techno-imaginaries: Any room for dialogue with Degrowth? *Journal of
Cleaner Production, 197*(2), 1694-1703.
<https://doi.org/10.1016/j.jclepro.2016.09.154>

[^25]: Braidotti, R. (2019). A Theoretical Framework for the Critical
Posthumanities. *Theory, Culture & Society, 36*(6), 31-61.
<https://doi.org/10.1177/0263276418771486>

[^26]: Táíwò, O. O. (2020, 12 August). Climate Apartheid Is the Coming
Police Violence Crisis. *Dissent*.
<https://www.dissentmagazine.org/online_articles/climate-apartheid-is-the-coming-police-violence-crisis>

[^27]: March, H. (2018). Op. cit.

[^28]: Ibid.

[^29]: Mastini, R., Kallis, G., & Hickel, J. (2021). A Green New Deal
without growth? *Ecological Economics, 179*.
<https://doi.org/10.1016/j.ecolecon.2020.106832>

[^30]: March, H. (2018). Op. cit.

[^31]: Goodchild van Hilten, L. (2019, 27 November). If we want to
survive on Earth, it\'s time to degrow. *Elsevier*.
<https://www.elsevier.com/connect/atlas/if-we-want-to-survive-on-earth-its-time-to-degrow>
