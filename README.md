# traduction OMSI 2020

Traduction française d’une sélection d’articles de l’édition 2020 de l’[Observatoire mondial de la société de l’information](https://www.giswatch.org/fr/) (OMSI), dont la langue de publication originelle est l’anglais.
