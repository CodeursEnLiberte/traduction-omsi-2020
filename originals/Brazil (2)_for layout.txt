Country: Brazil

Authors: Camila Nobrega[^1] and Eduardo Baptista Amorim[^2]

Organization: Intervozes - Coletivo Brasil de Comunicação Social

URL: [www.intervozes.org.br](http://www.intervozes.org.br)

Report title: **New technologies along the Tapajós and São Francisco
rivers in Brazil: A look at social and environmental justice narratives
in different territories**

**Introduction**

In some regions in Latin America, it is a huge challenge to be a
journalist or popular communicator in the midst of socio-environmental
conflicts. It is also risky to be a defender of your own territory. In
response to these difficulties, many have been turning to new
technologies: from community geomaps of traditional territories that
still struggle for recognition of collective entitlement to the land; to
internet and social networks to connect popular communicators in urban
and rural areas to build new collective forms of journalism and
communication in defence of territories; and even to drones to identify
land invaders. However, at what cost? There is no easy solution to
socio-environmental conflicts, nor the possibility of a universal or
one-size-fits-all response. 

Drawing on experiences along two rivers in Brazil -- the Tapajós River
in the state of Pará in the Amazon, and the São Francisco River in the
semi-arid Northeast region -- this report considers the deployment of
information and communications technologies (ICTs) for
socio-environmental justice and the right to territories.

Top-down techno-solutionism brings challenges and high risks. On the one
hand, the internet and other technologies are used by traditional
communities as part of their communication strategies in the struggle
for land and territory. On the other, this process is marked by many
hierarchies of power.[^3] For that reason, we pose two questions: What
asymmetries does the idea of a global environmental crisis hide?[^4] And
how can we build and recognise the already existing and less centralised
networks in communities used for the circulation of narratives, and
especially the cultural backgrounds of Black, Indigenous and traditional
communities?

**Context**

Since the beginning of 2019, Brazilian President Jair Bolsonaro has
attempted to deregulate environmental protection laws. During the
COVID-19 pandemic, this process was accelerated.[^5] Increasingly, the
country lives at a time in which communities using non-hegemonic ways of
social organisation need to develop ways to protect themselves and the
territories they historically occupy. The coronavirus has placed a
spotlight on inequalities and has resulted in the emergence of new ones,
as the government responds to the pandemic in a way that has been
investigated as genocidal. Among other problems with the government's
response, there is no public policy for prevention and care adapted to
local needs, nor proper access to information.[^6]

In this context, economic recovery decisions are made to the detriment
of the lives of people, especially those considered an obstacle to a
development package that is based on the expansion of megaprojects
related to activities such as agribusiness, mining and the use of other
so-called \"environmental resources\". As a result, marginalised
communities in urban areas, as well as traditional populations such as
*Quilombolas* (descendants of enslaved African people who managed to
escape and build communities), small farmers and riverside and fishing
communities have been disproportionately affected by the pandemic.
Brazil has one of the highest mortality rates for the pandemic in the
world, and there is a need for an extended quarantine of communities.
This has intensified the need to use technologies for communication
within communities and with the outside world. At the same time, land
conflicts have not stopped. On the contrary, environmental invasions and
devastation have increased. State discourses that are based on the need
to promote economic development in these areas have become stronger,
leading, among other things, to the massive deployment of technologies.
In September 2020, the current federal government even announced an
Amazon Rainforest internet programme, describing its aim as "to
integrate the largest green lung of the planet" and allowing the
installation of underwater fibre-optic networks that will run along the
Amazon rivers for 10,000 kilometres.[^7] But for what and for whose
needs?

The programme falls under the development proposal for the Amazon, which
is based on massive extraction of natural resources and the expulsion of
traditional peoples from their territories.

Building on the role that rivers and the communities living along them
play in the country, below we discuss some of the results of our
academic research and practices in specific places along the Tapajós and
São Francisco Rivers. Both are born from the waters of the Cerrado
biome[^8] in the centre of the country, but run in different directions:
to the North and the semi-arid Northeast. We point to some technological
challenges, such as access to and quality of the internet, and highlight
local strategies that are being developed to face risks. Our proposal of
contrasting these two cases is to find challenges and similarities, and
to build a critical vision. Our research goes far beyond the walls of
universities and is located in the practices we collectively develop at
Intervozes. We also discuss the necessity of improving communications in
order to struggle against extractive and invasive megaprojects and the
conflicts they bring and, at the same time, for disputing imaginaries of
memory, the present and the future.

**Rivers, radio transmitters and the internet: Communication flows**

*(By Camila Nobrega)*

The day I visited the community of Sawre Muybu in Médio Tapajós, in
2019, they were bringing back a drone which had fallen. The women from
the Munduruku Audiovisual Collective -- a collective founded by women --
were worried, but laughing, telling me about this new technological tool
in the struggle for the right to the Munduruku Indigenous people's
territory. The drone helps them map the activities of invaders of their
territory, such as deforesters, gold diggers and so on. The collective
also uses the internet to build their narratives on social networks, and
through other online documentation processes.

By travelling the Tapajós River and interviewing women from different
communities, it is possible to see the role that access to technologies
and the internet have played in different initiatives. Another one is
the Articulation of Communicators in Defence of Territories, which
connects people from different communities through multiple new and
traditional forms of communication. This includes radio, meetings and
assemblies where traditional knowledge is exchanged, and even dating
strategies through radio transmitters, added by internet connections.
These are multiple layers of communication that complement each other.
And what became evident is that the river is a means of communication in
itself.

Simultaneously, the pressures on traditional communities are growing
with the encroachment of agribusiness, deforestation, mining -- economic
sectors that are large-scale. More 43 hydroelectric plants could be
built along the Tapajós River and its tributary or formative rivers,
according to hydrographic basin inventory studies by the Brazilian
electricity regulatory agency. The plan was denounced at the United
Nations by traditional communities and the Federal Prosecutor\'s
Office.[^9] In general, the metrics used to account for the success of
these business activities and projects or their negative impacts are
guided by a logic decided far from the territories where they are
implemented. It is a logic of the scalability of non-scalable things, or
an alienation based on complex layers of distances, as the US
anthropologist Ana Tsing said.[^10] .

Traditional knowledge holders living in the Tapajós region bring a shift
to the narratives. During a talk we did together at the Discotech event
organised by APC at the 2019 Internet Governance Forum,[^11] the
Indigenous leader and lawyer Vândria Borari said in reference to the
International Labour Organization convention on Indigenous and tribal
peoples (No. 169): \"Our right to prior, free and informed consultation
has to be respected in any process. We know what we need in our
territories and what technology means to us.\"

Another Indigenous leader, Alessandra Munduruku, pointed out in an
interview in 2019: "Infrastructure projects are installed inside our
house \[the Amazon forest\] and we are the last to know." She added:
"And we don't want internet or anything, if it means destroying our
territory. We have to be heard when we say how we want things to be
done."[^12] 

They do not mean the that they do not see the benefit of technology. On
the contrary, both of them have pushed for access to the internet, in
their communities. Munduruku is part of a project that also uses drones
to protect their territory and simultaneously an enthusiast of
alternative forms of communication. Borari also calls herself a
communicator, using social networks and other means to communicate.

They do, however, suggest the need to think beyond access as a
framework, as academics like to say. Instead it is important to include
the perspectives of communities in the idea of "access", such as the
recognition of traditional epistemologies, and ways of knowledge
production and sharing. And, before all of that, the question needs to
be posed: How will access help to guarantee land rights, which is the
basis of the autonomy of the communities? The proposal is to reverse the
logic of problem solving that usually guides public policy. The focus is
instead on understanding \"where the problem is located\" and \"how to
identify its roots." So, if it is about access to the internet, it is
important to ask how this access is designed, and for whom it is
designed.

Instead of thinking about the end of the technological production chain
-- e-waste, for example -- it means reversing the angle to a bottom-up
way of thinking. For instance, this would involve thinking of the
technology sector's dependence on megaprojects involving energy or
mining, among other power relations that support the displacement of
peoples and territories. How are the notions of technology rooted in the
logic of development -- and therefore in the idea of keeping going with
"business as usual"?

At the same time, rethinking frameworks opens the possibility of
imagining ICTs as connected directly to land and territorial struggles.
From this perspective, it is not just about land, it is about
imaginaries and the recognition of traditional knowledge, science and
the multiplicity of forms of communication. In contrast to attitudes and
actions of the monopolies of big business, diversity in the territories
is directly connected to land and to our bodies.

**Networks to disseminate **

*(By Eduardo Amorim)*

Travelling in a car with Fundação Oswaldo Cruz (Fiocruz) signs in 2018,
to try to understand the disruptions and changes happening due to a
megaproject in the Northeast, I was able to reach Quilombo Santana, in
Salgueiro. The community was inaccessible, closed off by police
officers; journalists had no access to the people, because a few days
after a presidential visit and the opening of the São Francisco
Transposition project, some farmers lost their crops due to leaked water
from the channel. The engineering problems also threatened one of the
villages built to serve families removed from areas where the water
channel passed. We could only access the area because were driving a car
belonging to a federal research organisation: police officers had closed
roads and journalists could not access the region, where days earlier,
former president Michel Temer had been to open one of the phases of the
project.

Public misinformation was also used as a way to silence the problems of
the project.[^13] The silencing of how the people have suffered with
this project is frightening -- a project that the media and many
governments in Brazil long before Bolsonaro, including on the left, only
spoke about in a positive way. These public political positions have
been supported financially through agribusiness. Violence has been a
reality to whoever decides to talk about the corruption and the problems
of this mega-venture, no different from what has happened to the
traditional communities since the beginning of Portuguese occupation.

The biggest public organisation for health research in Brazil, Fiocruz
has over the past years being maintaining a portal called Beiras D'Água
("Water Borders"). Researcher André Monteiro and his team have aimed to
develop a space on the internet to spread the culture and ways of
resisting of the communities that live in the regions that are served by
the São Francisco River. For example, they use the portal to spread
videos about and made by Indigenous and Quilombola communities and the
struggle against the many hydroelectric plants that have killed most of
the fish in the region.

The portal includes video productions by artists and groups in the
Indigenous and Quilombola communities and Fiocruz videos. The main idea
is to take productions that were known only within a small group and
spread the narrative. The aim is always to help the project to create
awareness among Brazilians about the community narratives that are
silenced, but some issues, such as the evictions of families that lived
close to the transposition channels recently opened by Bolsonaro and the
loss of access to water due to this mega-venture, are censored in a way
that made the team from the research organisation start to produce some
videos, starting with *Invisíveis* (Invisible).[^14] Although the idea
behind the portal is to disseminate content that is produced by the
communities, how to deal when mega-ventures censor the issues in so many
different ways?

The Beiras D'Água initiative is not only a portal. It is starting to
become a group of people concerned about the environment and the river,
that has done research, filmed videos, planned film festivals and also
mobilised communities. The idea is similar to initiatives such as the
Mississippi River Anthropocene Curriculum,[^15] but has to deal with the
gap of technology literacy and especially the culture of violence and
the unfair reality of mass communication platforms such as TV channels
controlled by a few families in Northeast Brazil. As shown in the
fiction film *Bacurau*,[^16] communities are still fighting to have
access to water in the Northeast, but by developing networks to
disseminate those narratives, we need to think about the safety of the
leaders of those communities that have being violently silenced
historically in the region.

**Conclusion**

The escalation of socio-environmental conflicts in Brazil has led to the
rise of different communication networks connecting territories in which
ICTs play an important role. At the same time, however, the arrival of
these technologies often occurs in a top-down way, as part of a
development and ecological modernisation logic also responsible for
devastation, including the displacement of populations from their
territories.

From the two cases presented above, brought up from a perspective based
on socio- environmental justice and women\'s and feminist struggles in
the territories, we argue that it is necessary to shift perspectives,
both to evaluate the implementation of ICT projects and the impacts
involved and to seek new logics of collaboration and network building,
as well as recognition of territorial narratives and traditional,
ancestral technology.

Finally, the cases presented point to the fact that there is no single
global crisis: there are different perspectives of the systemic crises,
which need to be understood in the territorial dynamics. In this sense,
these observations are relevant to all of us who work in networks, as an
invitation to -- starting with the communities in the riversides[^17] --
deconstruct hegemonic, racist and heteronormative perspectives that
contribute to current intertwined socio-environmental devastations.

**Action steps**

The following action steps are necessary:

-   Connect the struggle for access to technologies with the right to
    territories.

-   Support the creation and growth of networks that strengthen groups
    on the edge of Brazilian rivers while preserving the role of
    riverside communities.

-   Insist on the right to consultation for communities as part of
    megaprojects or when technologies are implemented in those
    communities.

-   Shift narratives from sustainable development to socio-environmental
    justice and other views based on territorial diversities and that
    make power relations visible.

-   Map power asymmetries that develop when technologies are implemented
    in territories without the consent of communities.

-   Support local initiatives that promote the inclusion of communities.

-   Recognise different means of communication as technologies, not just
    new technologies. Recognise ancestral and traditional technologies
    as part of the present and the future, and not just the past.

-   Develop collective means of consultation on the implementation of
    communication and information technologies, building on existing
    documents developed by traditional communities.

-   Support women and LGBTIQ initiatives in urban and non-urban contexts
    and especially in traditional communities during mega-ventures which
    increase the male population in small cities and towns.

[^1]: Journalist and PhD researcher in Political Science at the Free
    University of Berlin, member of the Intervozes collective.

[^2]: Journalist and PhD researcher in Communications at Tulane
    University and UFPE, member of the Intervozes collective.

[^3]: To read more about power relations around implementation of the
    internet, see: Borari, V., & Nobrega, C. (2020). One Vision, One
    World. Whose World Then? *Branch, 1*.
    <https://branch-staging.climateaction.tech/2020/10/15/one-vision-one-world-whose-world-then>

[^4]: Drawing from the idea of environmental inequalities as put forward
    by the Brazilian anthropologist Andrea Zhouri. See: Zhouri, A.
    (2014). *Mapping Environmental Inequalities in Brazil: Mining,
    Environmental Conflicts and Impasses of Mediation*.
    desiguALdades.net International Research Network on Interdependent
    Inequalities in Latin America.
    <https://www.desigualdades.net/Working_Papers/Search-Working-Papers/working-paper-75-_mapping-environmental-inequalities-in-brazil_/index.html>

[^5]: Research done by Folha de S. Paulo and Instituto Talanoa shows
    that the federal executive branch published 195 acts related to
    environmental issues during the first months of the pandemic in
    Brazil. In the same months of 2019, only 16 acts were published on
    the same subjects. Amaral, A. C., Watanabe, P., Yukari, D., &
    Meneghini, M. (2020, 28 July). Governo acelerou canetadas sobre meio
    ambiente durante a pandemia. *Folha de S. Paulo*.
    <https://www1.folha.uol.com.br/ambiente/2020/07/governo-acelerou-canetadas-sobre-meio-ambiente-durante-a-pandemia.shtml>

[^6]: Saxena, S., & Costa, F. (2020, 20 July). Bolsonaro\'s Colossal
    Negligence Sparks \'Genocide\' Debate in Brazil. *The Wire*.
    <https://thewire.in/world/brazil-covid-19-jair-bolsonaro-genocide-negligence>

[^7]: La Prensa Latina. (2020, 2 September). Brazil's Bolsonaro
    announces Amazon rainforest internet program. *La Prensa Latina*.
    <https://www.laprensalatina.com/brazils-bolsonaro-announces-amazon-rainforest-internet-program>

[^8]: The tropical savanna of Brazil, a region widely exploited by
    agribusiness.

[^9]: Report by the Federal Prosecutor's Office to the UN Special
    Rapporteur on the rights of Indigneous peoples (in Portuguese):
    <http://www.mpf.mp.br/pa/sala-de-imprensa/documentos/2016/violacoes-direitos-povo-indigena-munduruku>

[^10]: Tsing, A. L. (2015). *The Mushroom at the End of the World: On
    the Possibility of Life in Capitalist Ruins*. Princeton University
    Press. <https://doi.org/10.2307/j.ctvc77bcc>

[^11]: [https://[videos]{.underline}.apc.org/u/adolfo/m/[discotech]{.underline}-environment-icts](https://videos.apc.org/u/adolfo/m/discotech-environment-icts/)

[^12]: Nobrega, C. (2019, 27 November). 'Everything is dying': Q&A with
    Brazilian indigenous leader Alessandra Munduruku. *Mongabay*.
    <https://news.mongabay.com/2019/11/everything-is-dying-qa-with-brazilian-indigenous-leader-alessandra-munduruku>

[^13]: Amorim, E. (2018, 30 November). Governo Federal distribuiu
    informações falsas para esconder as falhas técnicas da transposição.
    *Marco Zero Conteúdo*.
    [https://marcozero.org/governo-federal-distribuiu-informacoes-falsas-para-esconder-as-falhas-tecnicas-da-transposicao](https://marcozero.org/governo-federal-distribuiu-informacoes-falsas-para-esconder-as-falhas-tecnicas-da-transposicao/)

[^14]: [https://beirasdagua.org.br/item/invisiveis](https://beirasdagua.org.br/item/invisiveis/)

[^15]: <https://www.anthropocene-curriculum.org>

[^16]: <https://www.youtube.com/watch?v=1DPdE1MBcQc>

[^17]: Kelly, J., et al. (Eds.). (2017). *Rivers of the Anthropocene*.
    University of California Press. <https://doi.org/10.1525/luminos.43>
