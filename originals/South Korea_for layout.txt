Country: Korea, Republic of

Authors: Miru and Byoung-il Oh

Organization: Korean Progressive Network Jinbonet

URL: https://www.jinbo.net

**Title: Using technology: An evaluation of the climate crisis response
in South Korea**

**Introduction**

Due to the COVID-19 outbreak and extreme weather including heat waves
and floods that occur around the world, many countries are becoming more
aware of the climate crisis. The climate crisis is causing not only
environmental disasters, but also a serious economic downturn and social
inequality; so the response also calls for an overall reform of the
social and economic system. In line with this, major countries around
the world, including the United States and European nations, are setting
up "Green New Deal" strategies to cope with the climate crisis, break
down inequality and transform themselves into a decarbonised economic
society.

Korea is globally recognised as an internet powerhouse, but is also
criticised as a so-called "climate villain" because its response to the
climate crisis is insufficient. Information and communications
technologies (ICTs), including the internet, are both factors that
exacerbate the climate crisis in terms of consuming vast amounts of
energy, and also recognised as a useful means of coping with it. This
report aims to identify and evaluate issues related to Korea's ICT
policy in the context of responding to the climate crisis, focusing on
the Korean New Deal Comprehensive Plan.

**Background**

The Korean government has been promoting environment informatisation
policies -- which entail making databases from environment observation
data such as one on air quality during specific periods which helps to
anticipate climate -- to promote ICT-based environment-related public
works and provide a public service. The Ministry of Environment has
established and implemented a "basic plan for environmental
informatisation" every five years since 1997. It started with the first
basic plan (1997 to 2001), which focused on the establishment of basic
infrastructure and the informatisation of environmental affairs. The
fourth basic plan, which will be implemented from 2017 to 2021, is
planning to implement an intelligent environmental information system by
applying technologies of the so-called Fourth Industrial Revolution such
as big data, the internet of things (IoT) and artificial intelligence
(AI).[^1]

From the start of the Moon Jae-in administration, the government has
been promoting and nurturing Fourth Industrial Revolution technology and
industry as a key policy. The fourth basic plan reflects this. However,
the government is closer to carrying out its national information
service plan in general at the Ministry of Environment level rather than
use ICTs as part of its response to the climate crisis.

Korea's response to the climate crisis has not received a favourable
evaluation internationally. Since the signing of the Paris Agreement in
2015, the Korean government has also submitted a Nationally Determined
Contribution (NDC), a national greenhouse gas reduction goal, which was
37% of business as usual[^2] by 2030. In December 2016, the Office for
Government Policy Coordination established the First Basic Plan for
Climate Change Response and the 2030 Roadmap for Greenhouse Gas
Reduction. South Korea's NDC, however, was rated insufficient by the
Climate Action Tracker, and South Korea ranked among the world's top
four climate villains along with Saudi Arabia, Australia and New
Zealand.[^3]

The Moon Jae-in government adopted progressive policies such as
shrinking coal power plants and denuclearising power plants, but the
climate crisis response was still insufficient. The G20 Brown to Green
Report 2018,[^4] which provides concise and comparable information on
carbon emission mitigation measures, financial conditions and
vulnerabilities in G20 countries, showed that most of the G20 countries
generally have poor levels of response, but Korea's efforts are below
average. In the 2020 Climate Change Performance Index, which monitors
each country's climate protection performance, Korea ranked 33rd, the
lowest among 34 OECD countries after the United States. Overall, the
Korea index showed "very low" performance in 2020, similar to the
previous years. It means that no progress has been made on the
greenhouse emission and energy usage sectors that have been evaluated
very low.[^5]

Korean civic and environmental groups launched the "Climate Crisis
Emergency Action"[^6] on 21 September 2019, joined the global climate
strike and began to inform Korean society of the importance of climate
crisis issues. The emergency action group, which currently involves 377
organisations and individuals, has been active, continuously monitoring
environmental policies and holding forums to seek policy alternatives.

**The Korean New Deal**

Due to COVID-19, the Korean government faced an economic downturn and
mass unemployment in 2020. In order to remedy this, it has developed the
Korean New Deal, in part also in response to international pressure and
the demand of civil society to cope with the climate crisis.

The New Deal is defined as a decarbonised economic and social transition
aimed at coping with the climate crisis, breaking inequality and
creating green jobs.[^7] It is not necessarily new in Korea: even before
this crisis, in order to respond to a slowing of the economy, the Moon
Jae-in government sought to transition to the so-called "innovation
economy" centred on the promotion of new technologies, including big
data and AI.

The "Korean New Deal Comprehensive Plan" announced on 14 July 2020 has a
vision to transform Korea into a "leading economy from a chasing
economy, a low-carbon economy from a high-carbon dependency economy, and
an inclusive society from an unequal society." The comprehensive plan
includes a "strengthening safety net" policy to support both a Digital
New Deal and a Korean Green New Deal.

The United Nations Environmental Program (UNEP)[^8] proposed the Global
Green New Deal in 2009, and it has recently been proposed in the US and
Europe. In the US, more than 70 lawmakers, including Democratic
Congressperson Alexandria Ocasio-Cortez and Senator Ed Markey, submitted
a Green New Deal resolution in February 2019, and Joe Biden, who was
nominated as the Democratic presidential candidate, also presented the
Green New Deal as a pledge. In Europe, the European Commission announced
the "European Green Deal", which aims to achieve zero carbon emissions
by 2050. The Green New Deal was proposed as a pledge from the Justice
Party, the Green Party and the ruling Democratic Party during the April
2020 general election.

The Digital New Deal aims to promote digital innovation across the
economy by utilising the country's ICT base -- Korea's strength -- and
includes a total of 12 tasks, among them strengthening of its DNA (Data,
Network, AI) ecosystem. The Green New Deal aims to build a "net-zero"
economy and transform the economic base into a low-carbon and
eco-friendly one. It includes a total of eight tasks in three areas (see
below) and aims to strengthen the social safety net with respect to
employment and career transition. The government plans to invest 114.1
trillion won (about USD 102 billion) of state funds by 2025 to create
1.9 million jobs.

**ICTs and the Korean New Deal**

While tasks of the Digital New Deal directly advocate supporting the
data, 5G and AI industries, the Green New Deal policy also relies on new
technologies such as big data and AI. The Green New Deal policies
consist of eight tasks in three areas: 1) green transportation and
infrastructure, 2) low-carbon and decentralised energy supply, and 3)
innovation in the green industry.

For example, the so-called digital twin project aims to establish a
"digital twin" for roads, underground spaces, ports and dams to manage
the land and facilities safely, and it uses new technologies such as AI
and drones.[^9] The smart green industrial complex project also includes
the establishment of a system for remote monitoring of leakage of
harmful chemicals using AI and drones, and the establishment of smart
energy platforms that can collect data and visualise energy flows using
ICTs. It is also planning to make it mandatory to attach IoT measuring
devices to monitor the emission of pollutants at the workplace.

In the case of the task of digitising the public safety social overhead
capital, it will install next-generation intelligent transportation
systems (C-ITS)[^10] for major highways, IoT sensors for all railways,
and it will utilise non-face-to-face biometric systems, intelligent CCTV
and IoT at airports and ports.

**Criticism of the Korean New Deal**

However, civil society organisations in Korea are fiercely criticising
the Korean New Deal. Environmental groups criticise the comprehensive
plan by saying the "perception of the seriousness of the climate crisis
has disappeared, and we saw only the list of individual business
promotion plans."[^11] The biggest problem with the Korean Green New
Deal is that no clear goals have been set. The Climate Crisis Emergency
Action argues that the goal of reducing greenhouse gas emissions by
nearly half of the 2010 levels by 2030 and net-zero by 2050 as suggested
by the UN Intergovernmental Panel on Climate Change (IPCC) should be
clearly set. However, the Korean New Deal only contains the vague phrase
"pursuing a carbon neutral society" without providing a deadline for the
net-zero society.[^12] It also criticises the government's plan as
lacking a strategy to transform the social and economic system needed in
times of climate crisis. It says there is no mention of the reduction of
polluting industries such as coal power plants or internal combustion
engine vehicles, and that only measures to foster eco-friendly
industries are listed. While the true Green New Deal should have a "just
transition", it is hard to find a "just transition" for workers and
local residents in the Korean New Deal.

Jinbonet and other digital rights organisations have criticised the
Digital New Deal plan as "an attempt to boost economic growth by selling
people's personal information."[^13] Actually, the so-called DNA project
included in the Digital New Deal was already announced in an early stage
of the Moon Jae-in government. The revision of the Personal Information
Protection Act, which is the basis of the project, was made in January
2020. The main purpose of the revision is to allow companies to use
pseudonymised personal information for other purposes than the original
purpose, or to provide it to third parties without consent of the data
subjects. The government has argued that the utilisation of personal
information is needed to develop the big data and AI industries, but
civil society has criticised the revision as a "personal information
theft law".

Under the goal of developing Fourth Industrial Revolution technologies,
the comprehensive plan is unconditionally focusing on the use of new
technologies without any consideration on what the most important
problem in a particular field might be, and whether technology can help
solve the problem. For example, regarding the project establishing a
system for the remote monitoring of the leakage of harmful chemicals
based on AI and drones, the Korean Federation for Environmental Movement
(KFEM) raised questions about whether it is a project that properly
diagnosed and reflected problems at the site, by saying "unless it is a
very special case to see the fluidity of hazardous chemicals, measuring
them with AI and drones can be more dangerous."[^14]

**Conclusion**

The Korean Ministry of Environment has been pushing for environmental
informatisation for a long time, and recently started introducing new
technologies such as AI. These new technologies might be helpful to
enact environmental policies including coping with the climate crisis.
They can provide information to the public more easily, identify trends
that people have not perceived or enable the collection of detailed
environmental data. However, the Korean government's ICT policy is not
effectively linked to its climate crisis response policy.

First, although Korea has a good ICT infrastructure and data system, the
country has not been able to implement comprehensive and effective
greenhouse gas reduction and adaptation policies. One of the reasons is
that the projects initiated by various ministries such as the Ministry
of Economy and Finance (budget), Ministry of Trade, Industry and Energy
(energy), Ministry of Environment (greenhouse gas reduction), and
Ministry of Science and ICT (technology and data), are not effectively
linked with shared data and ICT infrastructure.

Second, despite the introduction of new technologies, ICTs often fail to
be linked to an actual climate crisis response programme in reality.
Technically, for example, combining weather information with energy
production data can predict future demand and supply, which can
significantly lower the power reserve ratio to reduce the use of fossil
fuels, but the Korean government is still unable to implement this, and
the power rates controversy repeats every summer.

Third, ICTs might help to respond to the climate crisis, but they might
accelerate the climate crisis at the same time. Jeremy Rifkin, the
author of *The Green New Deal: Why the Fossil Fuel Civilization Will
Collapse by 2028, and the Bold Economic Plan to Save Life on Earth*,
stresses that the use of ICT devices, as well as the manufacturing of
them and the infrastructure to maintain them, consume enormous amounts
of energy. He has insisted that the Green New Deal agenda must pay close
attention to decarbonisation of the ICT sector. In Korea, however, only
the use of ICTs is emphasised, and there is no concern about how to
minimise environmental problems caused by ICTs. Currently, a growing
number of companies have joined the global campaign "RE100" to replace
electricity with 100% eco-friendly renewable energy, but Korea has yet
to set standards for this, and the government is reluctant to demand
that the industry reduce greenhouse gas emissions in fear of weakening
national competitiveness.

Finally, under the current situation in Korea, the government's
perception of the seriousness of the climate crisis and its willingness
to achieve a shift in the socioeconomic system are more important than
the role of ICTs. This is because new technologies alone cannot solve
problems without clear goals and a specific roadmap for a decarbonised
society.

**Action steps**

The following steps are important for South Korea:

-   The government should clearly set goals for reducing greenhouse gas
    emissions and come up with a specific roadmap to implement them in
    accordance with the IPCC recommendation.

-   The Korean New Deal should be a plan to turn the social and economic
    system around in a way that is aligned with people's rights, and to
    that end, discussions should be held with all stakeholders,
    including local residents, civil society and workers.

-   In the course of the climate crisis response, ICTs should be
    introduced in a way that addresses problems but does not infringe on
    human rights and minimise the negative impact of ICTs on the climate
    crisis.

[^1]: Ministry of Environment. (2019). *Environmental White Paper 2019*.

[^2]: A scenario for future patterns of activity which assumes that
    there will be no significant change in people\'s attitudes and
    priorities, or no major changes in technology, economics or
    policies, so that normal circumstances can be expected to continue
    unchanged.

[^3]: Mathiesen, K. (2016, 4 November). South Korea leads list of 2016
    climate villains. *Climate Home News*.
    [https://www.climatechangenews.com/2016/11/04/south\_korea\_climate\_villains](https://www.climatechangenews.com/2016/11/04/south_korea_climate_villains/)

[^4]: Climate Transparency. (2018). *Brown to Green: The G20 Transition
    to a Low-Carbon Economy*. Climate Transparency, c/o
    Humboldt-Viadrina Governance Platform.
    <https://www.climate-transparency.org/g20-climate-performance/g20report2018>

[^5]: [[https://www.climate-change-performance-index.org/country/korea]{.underline}](https://www.climate-change-performance-index.org/country/korea)

[^6]: [[https://climate-strike.kr/]{.underline}](https://climate-strike.kr/)

[^7]: Lee, Y. (2020). *Green New Deal Evaluation and Improvement Plan*.

[^8]: United Nations Environment Programme. (2009). *Global Green New
    Deal: Policy brief*.
    [[https://www.unenvironment.org/resources/report/global-green-new-deal-policy-brief-march-2009]{.underline}](https://www.unenvironment.org/resources/report/global-green-new-deal-policy-brief-march-2009)

[^9]: A technology that predict results in advance by creating twins of
    real-life objects in a computer and simulating situations that can
    occur in reality.

[^10]: Cooperative Intelligent Transport System. Next-generation
    intelligent transportation seeking safety and convenience through
    mutual communication between cars or between cars and transportation
    infrastructure.

[^11]: Greenpeace. (2020, 14 July). A great disappointment at the
    half-baked Green New Deal.
    [[https://www.greenpeace.org/korea/press/14426/presslease-green-new-deal-statement]{.underline}](https://www.greenpeace.org/korea/press/14426/presslease-green-new-deal-statement/)

[^12]: Press release from the Climate Emergency Action about Green New
    Deal plan from the government: "Without a goal, it cannot respond to
    the climate crisis".
    [[https://climate-strike.kr/2601]{.underline}](https://climate-strike.kr/2601/)

[^13]: Statement: "Digital New Deal should be with information rights".
    [https://act.jinbo.net/wp/43213](https://act.jinbo.net/wp/43213/)

[^14]: KFEM statement: "To succeed in the Green New Deal, you need to
    complement your goals and challenges". <http://kfem.or.kr/?p=208426>
