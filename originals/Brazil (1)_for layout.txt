**Country: Brazil**

**Author: *Mariana Canto, Paula Côrte Real and André Ramiro***

**Organisation: Instituto de Pesquisa em Direito e Tecnologia do Recife
(IP.rec)**

**URL: https://ip.rec.br**

**Title: Walking through the fire: Open data and the environmental
crisis in Brazil**

Introduction 
============

Deforestation is a historical key concern in environmental policies in
Brazil. Among structural inequalities around sustainability in the
Amazon Rainforest, economic forces have always pressured for more areas
destined for the mining, logging and agriculture industries.[^1] Not
only natural resources face threats from the economic powers; peoples
and cultures are equally affected, as Indigenous and Quilombola
communities -- historically colonised and traditionally connected to
environmental preservation -- resist the growing deforestation.

As a strategy to preserve the rights of traditional communities and
monitor the sustainability of the environment, legal frameworks have
been created and technologies implemented over the last 20 years.
Protocols of transparency and open data were updated, taking into
account international standards, and the work of public and private
social entities became central to achieving environmental goals.

More recently, with the ascension of an authoritarian president, a
far-right-wing opponent of human rights,[^2] the sustainability of the
Amazon and the native peoples' rights agenda was deeply dismantled. In a
government distant from democratic practices, cases of public data
manipulation, censorship of research outcomes and negligence in relation
to the government's online platforms for transparency are a reality in
Brazil today.

The Sustainable Development Goals in Brazil have taken a step backwards
in decades. Because of this, the future of open information, the
environment and Indigenous culture are at great risk.

**Background**

Transparency, open data and access to information policies in Brazil,
including their expressions in environmental policy, have seen
considerable development over the past 20 years. This involved the
creation of the federal government\'s Transparency Portal in 2004; the
so-called Transparency Law in 2009; the Law on Access to Information in
2011; the foundation of the Open Government Partnership-- a partnership
between civil society and governments, of which Brazil is a founding
member -- in 2016; and the regulation of the Open Data Policy in Brazil
in 2016.

Policies for the protection of the environment and the demarcation of
Indigenous lands have also resulted in an institutional apparatus
specialised in guaranteeing the rights of native peoples and in the
inspection and publication of environmental data. Various institutions
and platforms form a network for observation and knowledge production,
such as the National Institute for Spatial Research (INPE), in charge of
monitoring deforestation in Brazil via satellites; the National Forestry
Information System (SNIF), a platform for collecting, processing,
storing and disseminating data on forests; the National Indigenous
Foundation (Funai); and the Climate Observatory.

**Takedown of data, voices, forests and lands**

However, the current administration\'s disrespect for human rights
directly impacts the dismantling of open data, environmental
preservation and policies on Indigenous rights.[^3] Jair Bolsonaro has
already referred to the Brazilian Indigenous peoples as \"smelly\",
\"animals in the zoo\"[^4] and \"a manipulated mass\"[^5] and said that
he would give weapons to farmers to "clear" these lands.[^6] Moreover,
Minister of Agriculture Ricardo Salles said that the government should
take advantage of the media attention focused on the coronavirus
pandemic and bypass the environmental legislation.[^7] Among numerous
other anti-environmental measures, Bolsonaro's administration ended the
Secretariat for Climate Change and two Amazon Fund Committees, in which
civil society entities participated as oversight stakeholders.[^8]

Censorship in research work and in the publication of open climate data
is a pattern in the Bolsonaro government and is directly related to its
pro-agribusiness agenda. In August 2019, INPE\'s director, Ricardo
Galvão, was discharged after the agency released data, collected via
satellite, which demonstrated record deforestation in the Amazon.[^9] In
July 2020, the release of new data on the high rates of forest
devastation resulted in the resignation of Lúbia Vinhas, researcher and
coordinator of the Earth Observatory, a department of INPE.[^10] In
addition, at the end of the first year of the Bolsonaro government, 43%
of the databases were out of date, in addition to several transparency
portals, such as SNIF's and Funai's websites.[^11]

**Forest and environmental devastation data burning**

Brazil is one of the top countries in terms of biodiversity and natural
resources: 58% of the country\'s total surface area is covered by
forests[^12] and natural biomes. It has the second highest forest area
in hectares according to the latest Global Forest Resources
Assessment.[^13] One of the country's most valuable environmental
resources is the Amazon Rainforest, the world's largest tropical forest,
which spans across eight different countries and has 64% of its total
area located in Brazil[^14] -- what is called the Brazilian Legal Amazon
(BLA)[^15] is made up of regions in nine Brazilian states.

Recently, Brazilian Amazon fires became the subject of significant
global attention due to an intense engagement in social media that
raised awareness of the increase of fires in 2019. After a massive smoke
cloud covered São Paulo\'s sky, it came to public attention that the
Amazon was \"burning\". The INPE claimed that the Amazon fires had
increased by 84% when compared to the same period in 2018.[^16]

As mentioned before, INPE\'s former director Ricardo Galvão[^17] was in
the middle of a controversy in 2019 due to the disclosure of data
related to a rise in the rate of environmental devastation. Right after
the data was released, Bolsonaro and the current Minister of the
Environment Ricardo Salles said that they would publicly contest it,
accusing Galvão of inconsistency and possible "damage" of "Brazil\'s
international image". INPE was founded in 1971 and Galvão had served at
the institute since 2016 -- he was also recognised by Nature[^18] as one
of \"ten people who mattered in science in 2019\" as a science defender.

Using satellites, INPE systematically maps the Legal Amazon and has been
responsible for generating yearly deforestation reports for the region
since 1988. As part of the Amazon and Other Biomes Monitoring Programme
(PAMZ+), different initiatives, such as the Programme for Monitoring the
Brazilian Amazon Forest by Satellite (PRODES) and the System of
Real-Time Deforestation Detection (DETER), were created by the institute
and represent a large database that is currently under threat.

With the aim to democratise data access and organise public databases,
INPE also developed TerraBrasilis,[^19] a web portal/platform with
geographic data generated by the above-mentioned programmes. One of the
platform\'s coordinators, Lúbia Vinhas, was recently removed from her
position and transferred to a \"new strategic role\", according to
Minister of Science and Technology Marco Pontes.[^20] However, Vinhas'
dismissal came days after the INPE's announcement of the increase in the
rates of deforestation, which showed an increase of 25% in comparison to
2019.[^21]

There are other information systems such as the National Environmental
Information System (SINIMA) that collects and organises Brazilian
natural resources data. The system is one of the instruments of the
National Environmental Policy, and is intended as a \"conceptual
platform based on the integration and sharing of information between
various existing systems\", created and maintained by the Brazilian
Forestry Service (SFB). The SFB is responsible for maintaining the
National Forest Information System (SNIF) that manages data on forests
in order to support evidence-based policies and projects for their
conservation.[^22] In 2019, the SFB was transferred from the Ministry of
Environment to the Ministry of Agriculture, Livestock and Supply[^23] in
a clear attempt to dismantle the environmental protection ecosystem and
weaken the Ministry of Environment.

Brazilian Environment Minister Ricardo Salles himself was involved in
several scandals related to undermining national regulation on
environmental protection. In early 2020, he called on the government to
push for the deregulation of environmental policies while people were
distracted by the coronavirus pandemic. This was captured in a video
that the Supreme Court ordered to be released due to an investigation
involving Bolsonaro. Following this, the Talanoa Institute and the
newspaper *Folha de São Paulo*[^24] mapped a significant increase in the
number of acts passed by the executive on environmental issues during
the pandemic.

Two other entities still linked to the Ministry of Environment are the
Brazilian Institute of Environment and Renewable Natural Resources
(IBAMA) and the Chico Mendes Institute for Biodiversity Conservation
(ICMBio). A recent report on the trafficking of wildlife that cites both
institutes has shown that the lack of data is a major problem in the
fight against animal trafficking in Brazil. "In terms of the domestic
illegal wildlife trade in Brazil," the report's authors state,
"up-to-date systematised figures, either official or academic, are not
available due to the fragmented, incomplete and often inconsistent
datasets held by the various governmental agencies.\"[^25] IBAMA and
ICMBio are key institutes involved in the production and organisation of
data on the preservation of Brazilian biodiversity, but are suffering
numerous changes to their internal structure and management bodies after
Bolsonaro took on the country\'s presidency.

Indigenous peoples in Brazil
============================

Created in 1967, the *Fundação Nacional do Índio* (Funai) is responsible for identifying, demarcating and registering the lands occupied by Indigenous peoples, promoting policies aimed at their sustainable development, and reducing the environmental impacts created by external agents. 
=============================================================================================================================================================================================================================================================================================

In 2006, according to data provided by Funai, Brazil had a population of approximately 345,000 "natives". However, in the 2010 census, 817,963 people declared themselves to be Indigenous people (IBGE, 2012).[^26] This sudden increase is explained by the change in the identifying criteria and not by demographic factors. Still, Brazil does not yet have a precise estimate of the Indigenous population in its territory.[^27] 
=======================================================================================================================================================================================================================================================================================================================================================================================================================================

Funai used to invest in open source solutions for the monitoring of Indigenous lands. Nowadays, maps are out of date and tabs on Funai\'s website related to "Social participation" and "Open data" are down.[^28] On the federal government\'s open data portal,[^29] a section called "CACI -- Mapping of Attacks Against Indigenous People" was the first time that crimes against Indigenous peoples were systematised and georeferenced.[^30] Rather than being updated annually, the last report on "Violence against Indigenous peoples in Brazil" dates from 2015 and the last data uploaded onto the platform refers to the murder of an Indigenous child in 2017.
===========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================

In Funai\'s extensive Information and Communications Technology (ICT) 2020-2022 Master Plan, *principles* such as "publicity and transparency" are included. Under "*guidelines"*, the plan mentions the adoption of information accessibility standards and less bureaucratic procedures to provide society with a set of information and tools aimed at the promotion of sustainable development and the cultural preservation of Indigenous peoples. The plan also encourages the adoption of free software. Under "*strategic objectives*", the delivery of ICT solutions that add strategic value to Funai, the viability of digital public services for society, and the promotion of transparency through the use of ICTs are listed. However, after a SWOT analysis,[^31] several external threats were identified in Funai\'s ICT structure. Changes in the federal government\'s public policy plan, budget restrictions, political instability with the risk of discontinuity in strategic plans previously established, and the withdrawal or termination of contractors involved in critical services were some of them.[^32]
==========================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================

Recent data from NGOs shows how the Indigenous peoples are suffering the loss of their lands with the growth of conflicts caused by the advance of agroindustry in Brazil. During the last years, a more aggressive advance of an export-oriented economy resulted in an increase in the number of deaths in land conflicts in country. After a reduction over the past 10 years, deaths have increased during Bolsonaro's administration.[^33]
===============================================================================================================================================================================================================================================================================================================================================================================================================================================

During the COVID-19 pandemic, in order to make up for the neglect of the
federal government, open data initiatives at the regional level have
been emerging across the country. In June 2020, students at the Federal
University of Rio Grande do Norte (UFRN) launched a platform in order to
document Indigenous communities and villages, but also to trace the
impact of the COVID-19 pandemic on Indigenous peoples.[^34] In order to
monitor the COVID-19 cases within Indigenous villages and surrounding
areas, the NGO Instituto Socioambiental (ISA) created an interactive
online platform, in which it is possible to monitor the progress of the
virus through Brazilian Indigenous reserves.[^35]

*Quilombolas* and Afro-Brazilians
=================================

*Quilombos or Quilombola* communities, in the past, were places of refuge formed from the union of fugitive slaves or the purchase of lands by freed slaves. Contemporary quilombos refer to the lands of descendants of these peoples, who live in communities characterised by subsistence agriculture and cultural practices that have a strong link with their African ancestry. 
====================================================================================================================================================================================================================================================================================================================================================================================

More than 15 million Quilombolas live in Brazil, fighting for the right of ownership of their lands enshrined in the Federal Constitution since 1988. Quilombolas can claim official recognition from the Fundação Cultural Palmares (FCP). The FCP is a Brazilian public entity linked to the Ministry of Culture, created in 1988. Its main mission is the preservation of Afro-Brazilian culture. 
====================================================================================================================================================================================================================================================================================================================================================================================================

However, the last update on FCP's open data portal related to the number of certified quilombos (Quilombola settlements) is from February 2019 and there is no visualisation tool created for this purpose yet. The only available documents are related to metadata information and a spreadsheet last updated at the beginning of 2019, despite the FCP's Open Data Plan 2017/2019, establishing the monthly updating of *quilombo* certification data. 
=========================================================================================================================================================================================================================================================================================================================================================================================================================================================

In 2018, an amended version of the FCP\'s Open Data Plan was released. The new plan seems to postpone, often up to one year, some of the deadlines previously agreed. Still, it seems that the deadlines have not been met. Some of the goals were the "implementation of automatic publishing of open data information" and a new Open Data Plan for 2020/2022. However, no documents containing these data seem to be available on the portal.
================================================================================================================================================================================================================================================================================================================================================================================================================================================

In 2019, Sérgio Camargo was appointed as the new president of the FCP by Bolsonaro. The appointee was known for posting racist comments on social networks, referring to the Black rights movement in Brazil as \"evil scum\" formed by \"bums\" and belittling African-based religions. Moreover, he promised to fire those who did not share his \"goal\" of firing \"leftists\", stating that "assembling a new extreme right team" is necessary.[^36]
=========================================================================================================================================================================================================================================================================================================================================================================================================================================================

Among the various committees already extinguished by Camargo is the Open Data Committee. He also started to concentrate the power of decisions that were previously taken collectively. Black movement organisations consider his measures "not only authoritarian, but totalitarian and highly dangerous for what remains of democracy" in Brazil.[^37]
========================================================================================================================================================================================================================================================================================================================================================

They also noted that there is no interest in the effective participation of organised civil society or in the creation of arenas for debate. Currently, on the FCP's website, the section aimed at the disclosure of information regarding the holding of public hearings, public consultations or other forms of encouraging popular participation does not contain any information.[^38]
==========================================================================================================================================================================================================================================================================================================================================================================================

Conclusion
==========

The building of a sustainable environment strategy takes decades of
cooperation, geopolitics, fomenting alliances for multistakeholder
governance, the implementation of technologies to provide continuous
open data, and, mainly, a strong promotion of peoples\' rights. On the
other hand, it takes a couple of years to disrupt environmental policy
and regulations and to sell lands and forests in the service of capital.
It is safe -- and unfortunate -- to say that the latter is the model
adopted by the current Brazilian government.

It has also become clear that scientific research work bothers those
whose goals involve manipulating or withholding public information.
These opaque and unethical policies are typical of authoritarian regimes
and historical enemies of freedom of expression. This scenario suggests
that it is more important than ever to reaffirm the necessity of public
and private funding for environment-related research and promoting open
debate and freedom of expression and opinion in the scientific and
academic field.

Indigenous and *Quilombola* peoples have their ancestry, lands and
cultural heritage at the core of their lives. In this case, the use of
technologies and open information best practices by the entities
responsible for their preservation can be fundamental for plural and
democratic oversight and thus for a sustainable and trustful future. The
very essence of the Brazilian identity is at stake if its Indigenous
people and environment are not prioritised over commercial interests.
The fight for their causes is a form of decolonisation and a
strengthening of the global South.

Action steps 
============

The following action steps are necessary in order to address the **open
data and environmental crisis in Brazil**:

-   Publicly defend the maintenance of open data portals and
    transparency platforms as enablers for the exercise of political
    rights of vulnerable groups and for the protection of the
    environment.

-   Encourage educational campaigns and events with the scope of
    popularising free software tools in all sectors of society,
    emphasising the importance of open data for achieving the
    Sustainable Development Goals and for Brazilian democracy.

-   Push the government for accurate data that has been withheld over
    the past few years, using the Access to Information Law and/or other
    legal instruments available in the Brazilian legal system.

-   Seek mobilisation at the regional and international level to prevent
    the current administration from dismantling institutions that
    protect the environment and vulnerable communities in Brazil. Demand
    reparations through instruments and remedies available at
    international courts and through international organisations.

[^1]: Barbosa, C (2019, 5 September). Pecuária é responsável por 80% do
    desmatamento na Amazônia, afirma pesquisadora. *Brasil de Fato.*
    [[https://www.brasildefato.com.br/2019/09/05/pecuaria-e-responsavel-por-80-do-desmatamento-na-amazonia-afirma-pesquisadora]{.underline}](https://www.brasildefato.com.br/2019/09/05/pecuaria-e-responsavel-por-80-do-desmatamento-na-amazonia-afirma-pesquisadora)

[^2]: Amnesty International. (2020). *Human rights in the Americas:
    Review of 2019*.
    <https://www.amnesty.org/download/Documents/AMR0113532020ENGLISH.PDF>

[^3]: Silva, R. A. (2020, 11 February). As 26 principais violações ao
    meio ambiente feitas por Jair Bolsonaro. *Carta Capital.*
    [[https://www.cartacapital.com.br/blogs/brasil-debate/as-26-principais-violacoes-ao-meio-ambiente-feitas-por-jair-bolsonaro]{.underline}](https://www.cartacapital.com.br/blogs/brasil-debate/as-26-principais-violacoes-ao-meio-ambiente-feitas-por-jair-bolsonaro/)

[^4]: Marques, C. J. (2020, 14 February). Bolsonaro se lixa para os
    índios. *Istoé.*
    [[https://istoe.com.br/bolsonaro-se-lixa-para-os-indios]{.underline}](https://istoe.com.br/bolsonaro-se-lixa-para-os-indios/)

[^5]: Soares, I. (2020, 7 June). Bolsonaro critica demarcações e diz que
    índio \"sempre foi massa de manobra. *Correio Braziliense*
    [[https://www.correiobraziliense.com.br/app/noticia/politica/2020/06/07/interna\_politica,861849/bolsonaro-critica-demarcacoes-e-diz-que-indio-sempre-foi-massa-de-man.shtml]{.underline}](https://www.correiobraziliense.com.br/app/noticia/politica/2020/06/07/interna_politica,861849/bolsonaro-critica-demarcacoes-e-diz-que-indio-sempre-foi-massa-de-man.shtml)

[^6]: [[https://www.youtube.com/watch?v=jUgDXVbPHZs]{.underline}](https://www.youtube.com/watch?v=jUgDXVbPHZs)

[^7]: Greenpeace Brasil. (2020, 23 May). Ricardo Salles deve ser
    retirado imediatamente do Ministério de Meio Ambiente. *Greenpeace.*
    [[https://www.greenpeace.org/brasil/blog/ricardo-salles-deve-ser-retirado-imediatamente-do-ministerio-de-meio-ambiente]{.underline}](https://www.greenpeace.org/brasil/blog/ricardo-salles-deve-ser-retirado-imediatamente-do-ministerio-de-meio-ambiente/)

[^8]: Brasil de Fato. (2020, 5 June). \"Passar a boiada\": política
    ambiental de Bolsonaro é alvo de ações na Justiça. *Brasil de Fato.*
    [[https://www.brasildefato.com.br/2020/06/05/passar-a-boiada-politica-ambiental-de-bolsonaro-e-alvo-de-acoes-na-justica]{.underline}](https://www.brasildefato.com.br/2020/06/05/passar-a-boiada-politica-ambiental-de-bolsonaro-e-alvo-de-acoes-na-justica)

[^9]: Novaes, M. (2019, 2 August). "Constrangimento" com Bolsonaro por
    dados de desmatamento derruba diretor do Inpe. *El País.*
    [[https://brasil.elpais.com/brasil/2019/08/02/politica/1564759880\_243772.html]{.underline}](https://brasil.elpais.com/brasil/2019/08/02/politica/1564759880_243772.html)

[^10]: Kafruni, S. (2020, 13 July). Coordenadora do Inpe é exonerada
    após dado de devastação desmentir governo. *Correio Braziliense.*
    [[https://www.correiobraziliense.com.br/app/noticia/politica/2020/07/13/interna\_politica,871770/coordenadora-do-inpe-e-exonerada-apos-dado-de-devastacao-desmentir-gov.shtml]{.underline}](https://www.correiobraziliense.com.br/app/noticia/politica/2020/07/13/interna_politica,871770/coordenadora-do-inpe-e-exonerada-apos-dado-de-devastacao-desmentir-gov.shtml)

[^11]: Lima, R. (2020, 7 January). Governo federal fecha ano com 43% das
    bases de dados defasadas. *Metropoles.*
    [[https://www.metropoles.com/brasil/governo-federal-fecha-ano-com-43-das-bases-de-dados-defasadas]{.underline}](https://www.metropoles.com/brasil/governo-federal-fecha-ano-com-43-das-bases-de-dados-defasadas)

[^12]: FAO. (2020). *Final evaluation of "Strengthening national policy
    and knowledge framework in support of sustainable management of
    Brazil's forest resources".* Project Evaluation Series, 04/2020.

    [[https://www.fao.org/documents/card/en/c/CA8491EN]{.underline}](https://www.fao.org/documents/card/en/c/CA8491EN)

[^13]: FAO. (2020). *Global Forest Resources Assessment 2020: Main
    report*.
    [[https://www.fao.org/documents/card/en/c/ca9825en]{.underline}](https://www.fao.org/documents/card/en/c/ca9825en)

[^14]: Global Forest Atlas. (2020). *The Amazon Basin Forest*. Yale
    School of the Environment.
    [[https://globalforestatlas.yale.edu/region/amazon]{.underline}](https://globalforestatlas.yale.edu/region/amazon)

[^15]: The Brazilian Legal Amazon (BLA) corresponds to the area under
    the responsibility of the Brazilian Superintendency of the
    Development of the Amazon (SUDAM). The BLA is the region formed by
    the states of Acre, Amapá, Amazonas, Pará, Rondônia, Roraima,
    Tocantins and Mato Grosso, and by municipalities of the state of
    Maranhão.
    [[https://www.ibge.gov.br/en/geosciences/maps/regional-maps/17927-legal-amazon.html?=&t=o-que-e]{.underline}](https://www.ibge.gov.br/en/geosciences/maps/regional-maps/17927-legal-amazon.html?=&t=o-que-e)

[^16]: BBC. (2019, 21 August). Amazon fires increase by 84% in one year
    - space agency. *BBC*.
    [[https://www.bbc.com/news/world-latin-america-49415973]{.underline}](https://www.bbc.com/news/world-latin-america-49415973)

[^17]: Phillips, D. (2019, 2 August). Brazil space institute director
    sacked in Amazon deforestation row. *The Guardian.*
    [[https://www.theguardian.com/world/2019/aug/02/brazil-space-institute-director-sacked-in-amazon-deforestation-row]{.underline}](https://www.theguardian.com/world/2019/aug/02/brazil-space-institute-director-sacked-in-amazon-deforestation-row)

[^18]: [[https://www.nature.com/immersive/d41586-019-03749-0/index.html]{.underline}](https://www.nature.com/immersive/d41586-019-03749-0/index.html)

[^19]: [[http://terrabrasilis.dpi.inpe.br]{.underline}](http://terrabrasilis.dpi.inpe.br/)

[^20]: R7. (2020, 14 July). Governo diz que responsável pela Amazônia
    não foi demitida do INPE. *R7.*
    [https://noticias.r7.com/brasil/governo-diz-que-responsavel-pela-amazonia-nao-foi-demitida-do-inpe-14072020]{.underline}

[^21]: Spring, J. (2020, 13 July). Brazil reassigns deforestation data
    manager, raising question of political influence. *Reuters.*
    [[https://www.reuters.com/article/us-brazil-environment/brazil-reassigns-deforestation-data-manager-raising-question-of-political-influence-idUSKCN24F025]{.underline}](https://www.reuters.com/article/us-brazil-environment/brazil-reassigns-deforestation-data-manager-raising-question-of-political-influence-idUSKCN24F025)

[^22]: [[http://snif.florestal.gov.br/pt-br/o-que-e-o-snif]{.underline}](http://snif.florestal.gov.br/pt-br/o-que-e-o-snif)

[^23]: [[http://www.planalto.gov.br/ccivil\_03/\_ato2019-2022/2019/Lei/L13844.htm]{.underline}](http://www.planalto.gov.br/ccivil_03/_ato2019-2022/2019/Lei/L13844.htm)

[^24]: Amaral, A. C., Watanabe, P., Yukari, D., & Meneghini, M. (2020,
    28 July). Governo acelerou canetadas sobre meio ambiente durante a
    pandemia. *Folha de S. Paulo*
    [[https://www1.folha.uol.com.br/ambiente/2020/07/governo-acelerou-canetadas-sobre-meio-ambiente-durante-a-pandemia.shtml]{.underline}](https://www1.folha.uol.com.br/ambiente/2020/07/governo-acelerou-canetadas-sobre-meio-ambiente-durante-a-pandemia.shtml)

[^25]: Charity, S., & Ferreira, J. M. (2020). *Wildlife Trafficking in
    Brazil*. TRAFFIC International.
    [[https://www.traffic.org/site/assets/files/13031/brazil\_wildlife\_trafficking\_assessment.pdf]{.underline}](https://www.traffic.org/site/assets/files/13031/brazil_wildlife_trafficking_assessment.pdf)

[^26]: IBGE. (2012). *Os indígenas no Censo Demográfico 2010: primeiras
    considerações com base no quesito cor ou raça*. IBGE.
    [[https://indigenas.ibge.gov.br/images/indigenas/estudos/indigena\_censo2010.pdf]{.underline}](https://indigenas.ibge.gov.br/images/indigenas/estudos/indigena_censo2010.pdf)

[^27]: There are numerous records of sightings of Indigenous
    "uncontacted" peoples living in voluntary isolation. In 2020, the
    Funai database had 28 confirmed records of uncontacted groups and 86
    records under analysis, totalling 114 groups. Several of these
    sightings occurred within protected reserves, but other groups are
    exposed in regions that are under great environmental pressure from
    farmers, miners and the agricultural industry, making their fate
    very uncertain.

[^28]: [[http://www.funai.gov.br/index.php/acesso-a-informacao2/mn-dados-abertos]{.underline}](http://www.funai.gov.br/index.php/acesso-a-informacao2/mn-dados-abertos)

[^29]: [[http://dados.gov.br/aplicativos?organization=ministerio-da-justica-e-seguranca-publica-mj]{.underline}](http://dados.gov.br/aplicativos?organization=ministerio-da-justica-e-seguranca-publica-mj)

[^30]: [[http://caci.cimi.org.br]{.underline}](http://caci.cimi.org.br/)

[^31]: The term SWOT is an acronym for \"strengths\", \"weaknesses\",
    \"opportunities\" and \"threats\". It is a method that makes it
    possible to verify and evaluate the intervening factors for a
    strategic positioning of the ICT unit in the environment in
    question.

[^32]: Jair Bolsonaro\'s attempts to dismantle the Indigenous protection
    system are numerous. Hours after taking office on 1 January 2019, he
    tried to transfer the demarcation of Indigenous lands to the
    Ministry of Agriculture. In February 2020, Bolsonaro appointed a
    former evangelical missionary, who is linked to an organisation
    known for forcing contact with Indigenous groups and trying to
    evangelise them, as general coordinator of Funai's Isolated
    Indigenous Peoples.

[^33]: Sugimoto, L. (2019, 18 March). Geógrafo alerta para desmonte da
    Funai. *UNICAMP*.
    [[https://www.unicamp.br/unicamp/index.php/ju/noticias/2019/03/18/geografo-alerta-para-desmonte-da-funai]{.underline}](https://www.unicamp.br/unicamp/index.php/ju/noticias/2019/03/18/geografo-alerta-para-desmonte-da-funai)

[^34]: [[https://cchla.ufrn.br/povosindigenasdorn/mapa.html]{.underline}](https://cchla.ufrn.br/povosindigenasdorn/mapa.html)

[^35]: [[https://covid19.socioambiental.org]{.underline}](https://covid19.socioambiental.org/)

[^36]: Terra. (2020, 2 June). Movimento negro é \"escória maldita\", diz
    Sérgio Camargo. *Terra.*
    [[https://www.terra.com.br/noticias/brasil/politica/movimento-negro-e-escoria-maldita-diz-sergio-camargo,c40ff8b50aac1fa2ed55593eabee7e8aj1um9xrw.html]{.underline}](https://www.terra.com.br/noticias/brasil/politica/movimento-negro-e-escoria-maldita-diz-sergio-camargo,c40ff8b50aac1fa2ed55593eabee7e8aj1um9xrw.html)

[^37]: Amado, G. (2020, 25 June). Presidente da Fundação Palmares não
    recebeu movimentos negros. *Época*.
    [[https://epoca.globo.com/guilherme-amado/presidente-da-fundacao-palmares-nao-recebeu-movimentos-negros-24497003]{.underline}](https://epoca.globo.com/guilherme-amado/presidente-da-fundacao-palmares-nao-recebeu-movimentos-negros-24497003)

[^38]: [[http://www.palmares.gov.br/?page\_id=55960]{.underline}](http://www.palmares.gov.br/?page_id=55960)
