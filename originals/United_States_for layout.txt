Country: United States

Title: A Digital Tech New Deal: Digital socialism, decolonisation, and
reparations for a sustainable global economy

Author: Michael Kwet

Organisation: Visiting Fellow, Information Society Project, Yale Law
School

Email: mike\@mikekwet.com /
[michael.](mailto:michael.kwet@yale.edu)[kwet\@yale.edu](mailto:michael.kwet@yale.edu)

Introduction: The environmental crisis and inequality

As we progress into the 21st century, the human race is driving planet
Earth towards ecosystem collapse. Scientists fear that because humans
are overheating the environment and overconsuming its material
resources, we are generating a sixth extinction event that is
extinguishing billions of animals. Without a rapid change in the way we
conduct global civilisation, we will destroy much of life on Earth,
including, potentially, our own species.

As scholars have long noted, capitalism, with its pursuit of profit and
infinite growth, is the force driving the climate crisis.[^1] From an
eco-socialist perspective, institutional problems include global
inequality, corporate power, and a growth-oriented and profit-centred
model of development. During the neoliberal era (late 1970s-present), if
we disaggregate China out from global economy metrics, inequality
between the global North and the global South has increased, with Africa
and South Asia losing the most ground.[^2] At present, 58% of all people
live on less than USD 7.40 per day, the meagre global poverty line
required to achieve normal life expectancy.[^3]

As political economist Sean Starrs has demonstrated, despite gains made
by China -- largely on the backs of exploited labour -- the United
States remains at the pinnacle of global wealth and power. In the
post-World War II period, US power globalised; its transnational
corporations dominate nearly every sector of the world economy.[^4]

The global inequality created by capitalism threatens the environment.
Its insatiable appetite for profit and growth -- a structural imperative
-- is environmentally unsustainable, not only because it will likely
overheat the planet, but also because it is overconsuming material
resources from the Earth. For decades, advocates of capitalism have
argued that unequal growth is acceptable so long as the poor make
marginal gains. Yet projections show that on the current capitalist
growth model -- about 2-3% annually -- to achieve the extent of growth
needed to eradicate global poverty, measured at a mere USD 5 per day,
the global GDP would have to increase to 175 times its present size.[^5]

The problem with capitalism, degrowth advocates observe, is that we can
no longer fatten the pockets of those who are wealthy on the global
scale, from the middle classes on up. Rather, we need to rapidly grow
the livelihoods of the poor, redistribute and reduce the consumption of
the well-off, and set the global economy into a balanced
equilibrium.[^6] This is a monumental task, as the rich and powerful --
led by the US power elite -- are marching ahead towards profit and
growth, dragging the rest of life on Earth with them towards imminent
destruction.

How digital colonialism threatens the environment

Within this cauldron of affairs, we now have Big Tech. In the US, the
top five tech transnationals, GAFAM (Google/Alphabet, Apple, Facebook,
Amazon and Microsoft), are collectively worth over USD 5 trillion. The
Big Tech behemoths have concentrated wealth on the basis of owning the
digital ecosystem -- software, hardware, and network connection -- the
core infrastructure of the digital world.

As with the prior era of capitalist expansion, a new wave of
corporations -- mostly US transnationals -- are colonising the global
economy through the process of digital colonialism. At root, digital
colonialism is about the ownership and control of the digital ecosystem
for political, economic and social domination of a foreign
territory.[^7]

Under classic colonialism, Europeans seized and settled foreign land;
installed infrastructure like railroads and sea ports; constructed heavy
machinery and exploited labour used to extract raw materials; erected
panoptic structures to police workers; marshalled the engineers needed
for advanced economic exploitation; shipped the raw materials back to
the mother country for the production of manufactured goods; undermined
global South markets with cheap manufactured goods; perpetuated
dependency of global South peoples in an unequal global division of
labour; and expanded market, diplomatic and military domination for
profit and plunder.

Today, the "open veins" of the global South are the "digital veins"
crossing the oceans, wiring up a tech ecosystem owned and controlled by
a handful of mostly US-based corporations. The transoceanic cables are
often fitted with strands of fibre owned by the likes of Google and
Facebook, for the purpose of data extraction and monopolisation. The
cloud centres are the heavy machinery dominated by Amazon and Microsoft,
proliferating like military bases for US empire, with Google, IBM and
Alibaba following behind. The engineers are the corporate armies of
elite programmers numbering in the hundreds of thousands, with generous
salaries of USD 250,000 or more as compensation.

The exploited labourers are the people of colour producing the minerals
in the Congo and Bolivia, the armies of cheap labour annotating
artificial intelligence data in China and Africa, the East Asian workers
enduring post-traumatic stress disorder (PTSD) to cleanse Big Social
Media of graphic content, and the vast majority of people asked to
specialise in non-digital goods and services in a worldwide division of
labour. The centralised intermediaries and spy centres are the
panopticons, and data is the raw material processed for artificial
intelligence services.

The US is at the helm of advanced economic production, which it
dominates through the ownership of intellectual property and core
infrastructure, backed by imperial trade policies at the World Trade
Organization. The missionaries are the World Economic Forum elites, the
CEOs of Big Tech corporations, and the mainstream \"critics\" in the US
who dominate the "resistance" narrative, many of whom work for or take
money from corporations like Microsoft and Google, and integrate with a
network of US-Eurocentric intellectuals drawn from elite Western
universities. Added to this, state-corporate elites, entrepreneurs, and
educational institutions in the global South are replicating the Silicon
Valley model of digital capitalism.

This problem intersects with the environment, compounding challenges to
developing a sustainable economy, for a number of reasons. First,
digital capitalism concentrates wealth. We have already seen the
technology industry create new monopolies, ultra-wealthy oligarchs,
exploit and threaten to undermine labour through gig labour, and
gentrify cities like San Francisco.[^8] In most parts of the world,
US-based transnational corporations dominate the broad range of products
and services in the digital ecosystem.[^9]

As digital technology spreads, if wealth further accrues to the Silicon
Valley colonial metropole and elites within global South countries, then
wealth inequality will further obstruct a just and sustainable
resolution to the environmental crisis.

Second, the Silicon Valley model of digital society includes the use of
powerful new technologies to police communities. At this moment in
history, we need radical transformation of the status quo, which
requires a radical redistribution of wealth and power. Yet throughout
history, we have seen those with power use technology as tools to
suppress social justice movements. Big Tech corporations like Microsoft,
Amazon and Google are partnering with a shadow industry of corporations
to provide law enforcement agencies and the US military to service
police and US empire, including in countries like South Africa, Brazil
and India.[^10]

Third, the Silicon Valley business model is broken. There are two
primary ways Big Tech makes money. The first is to charge users for
using their technology. This requires them to offer a product you either
purchase by the unit (such as proprietary software you install on your
computer) or subscribe to (such as software owned and controlled by
corporations running in their cloud). The second is to force-feed users
ads and/or monetise surveillance.

This capitalist model poses numerous problems. If people have to pay
out-of-pocket for tech services, then the world's poor majority will be
excluded, because they have very little or no disposable income.
Moreover, in order to compel payments or force-feed ads, the technology
has to be owned and controlled by the product or service provider,
giving them the power to exercise control over the users, who would
otherwise resist the ads.[^11] This problem also inheres with the
enforcement of copyright, which requires draconian control over the
means of computation to prevent people from copying and sharing
published works without paying on the market.[^12]

Fourth, the constant stream of advertisements pushed at users provokes
consumerism at a time when we have to shift from a consumerist lifestyle
to a societal orientation which values free time, leisure, creativity
and spiritual fulfilment.

Fifth, an enormous amount of waste goes into efforts to manipulate
people with corporate consumerist propaganda, including labour time to
run, execute and develop AdTech and useless Big Data technologies, as
well as the computer storage capacity, and computer processing in the
cloud dedicated to wasteful products and services. With the internet of
things (IoT), we will allegedly build internet-connected technology into
all of our "things", from baby diapers to toothbrushes and toasters. IoT
providers see a new market to continuously replace and "upgrade" our
everyday items, instead of building them to last.

**People's Tech: Digital socialism and a Digital Tech New Deal**

To fix these problems, we need to build a socialist tech ecosystem that
produces for need instead of exchange value, equality instead of profit
and power, and sustainability instead of endless growth. In other words,
we need to develop digital socialism.[^13]

Any tech solution we introduce must be green and within the parameters
of sustainable material throughput. The following solutions, therefore,
must be developed within the context of green energy and material
limitations -- a production challenge in immediate need of study and
attention.

Drawing from the free/libre and open source software (FLOSS) philosophy
and movement, I have suggested that we develop and expand "People's Tech
for People's Power" -- a digital ecosystem based on a free software and
internet decentralisation, supported by socialist legal solutions,
critical education, grassroots movements, and bottom-up democracy.[^14]

On a "People's Tech" model, a set of interlocking solutions would
transform the digital ecosystem. Software would be free and open sourced
under strong copyleft licences, which require the disclosure of modified
source code downstream as software develops. Copyleft ensures that the
software commons is not enclosed by private owners and remains available
for anyone to use, study, modify and distribute, for free. Wherever
possible, cloud-based services like social media networks and platforms
would be decentralised, interoperable and open sourced, so that there is
no centralised intermediary in control. Legal solutions, including new
laws and regulatory bodies, would support this arrangement, to ensure
that the people directly own and control the networks as a democratic
commons. Knowledge would be freely accessible to everyone on equal
terms.

Strong privacy rights would ban surveillance imposed on workers,
students, teachers and members of the public, including by law
enforcement agencies and city authorities for "safe" and "smart" cities.
Technology systems would be developed *not* to collect data by their
very design, or keep it to a minimum where it is absolutely necessary
(i.e. privacy by design). The right to repair and product design for
longevity would help reduce e‑waste and ensure compatibility with
degrowth objectives.

Resources for infrastructure and development would be extended to people
in the global South as reparations for colonialism and slavery,
including recent revenue extraction through digital colonialism. Big
Tech corporations -- and corporations in general -- would be phased out
of existence. Their property, both intellectual and physical, would be
socialised for democratic self-management and collective ownership.[^15]
Production and consumption of goods and services would be coordinated
locally, regionally and globally for wealth and income redistribution.
Advertising and consumerism would be abolished, as would wasteful
production geared towards overconsumption and behavioural manipulation.

To set this agenda in motion, it will take a committed grassroots
movement that intersects with other social justice movements. There are
three ideological forces standing in the way.

First, ruling class elites, especially in the US, will do everything in
their power to prevent it from happening.

Second, other elites, including those at the World Economic Forum
pushing the Fourth Industrial Revolution, will continue to pressure
their own societies to adopt digital capitalism, for the gain of local
elites, often in collaboration with US and other powerful actors.

And third, this will require challenging the US "soft left", where the
dominant "resistance" narrative has been formulated by a liberal
imperialist "techlash" that claims it is critical of Big Tech, but
focuses on a narrow set of problems, such as algorithmic bias, facial
recognition, unionising US tech workers (without challenging private
property or digital colonialism), weak "privacy" laws (like the EU\'s
General Data Protection Regulation and the US's California Consumer
Privacy Act), content moderation, and US-based antitrust for
"competitive" markets. In this worldview, problems revolve around making
Big Tech nicer -- much like the Sullivan Principles during
apartheid[^16] -- instead of eradicating Big Tech, corporations and
capitalism, including in its digital form. This "tech ethics" circuit is
dominated by US-Eurocentric researchers working for or taking money from
corporations like Microsoft and Google, academics at elite Western
universities, prominent NGOs, wealthy foundations, and big corporate
media outlets, who together form a connected network and shared
ideology.[^17]

Conclusion

The prospect of backlash from resistance to the US tech empire is
enormous, and activists and scholars must build solidarity across the
world. Pressure must be centred on the US to change its behaviour.
Activists and intellectuals must develop a different, more principled
path on the digital society if they are to avert ecological breakdown
and global catastrophe. They cannot take their cues from the US soft
left. Sustainable development requires the rapid breakdown of
capitalism. This includes its dominant, authoritarian institution, the
corporation; intellectual property; and the private ownership of
infrastructure like software, cloud server farms, minerals and
networking hardware.

Allies will emerge -- including some in the West -- if a clear and
principled message of resistance is articulated by a grassroots movement
working from below. Policies and activism cannot be developed in
isolation -- intellectuals, activists, unions and policy makers in
government must come to the table and form eco-socialist legal solutions
in tandem with others across the world.

People on the ground have nothing to lose but their chains. Google,
Apple, Facebook, Amazon and Microsoft, as well as the other Big Tech
corporations -- including Chinese giants like Huawei and Alibaba -- are
colonising the digital landscape. Global South corporations and
entrepreneurs are following suit. The challenges are difficult, but must
be overcome.

There is a rich history of resistance to digital colonialism for
activists to draw upon. During South African apartheid, the world's
people called for boycotts, divestment and sanctions (BDS) against
corporations like IBM and Hewlett Packard, which aided and abetted the
apartheid government and businesses. US corporations, in response,
pushed a reformist agenda called the Sullivan Principles said to improve
worker conditions. Anti-apartheid activists rejected the Sullivan
Principles as corporate propaganda designed to manufacture consent while
US corporations continued to profit from apartheid misery.

The movement against digital colonialism needs to be resurrected to meet
the current environment. This time, the US fully occupies the centre,
through its endless pursuit of racialised economic and political
domination, which is driving the environment to the brink of collapse.

Action steps

The following action steps should be taken to decolonise tech for
environmental sustainability:

-   *Implement People's Tech:* Develop and replace Big Tech products and
    services with free software and decentralisation technologies.
-   *Create socialist legal solutions:* Push for laws and regulations to
    socialise the digital ecosystem as a socialist commons based on
    direct democracy. Demand wealth redistribution and reparations.
-   *Unite for anti-colonial resistance:* Globally unite to resist
    digital colonialism. Consider boycott, divestment and sanctions
    targeting Big Tech, especially Silicon Valley.
-   *Union power:* Unions should put People's Tech on the workplace
    agenda and develop critical consciousness. Non-technology workplaces
    should put pressure on tech corporations and workers to stop
    developing exploitative technologies.
-   *Educate:* Replace Big Tech products and services with People's Tech
    in schools and universities. Debate, learn, and develop critical
    consciousness about tech and society.
-   *Green tech:* Study how to ensure tech is green and environmentally
    sustainable. Produce and distribute technology within the bounds of
    ecological sustainability.

[^1]: Williams, C. (2015). Marxism and the environment. *International
    Socialist Review, 72*.
    <https://isreview.org/issue/72/marxism-and-environment>; Weber, J.
    (1950). The Great Utopia. *Contemporary Issues: A Magazine for a
    Democracy of Content*, *2*(5); Bookchin, M. (1962). *Our Synthetic
    Environment*.
    <https://libcom.org/files/Bookchin%20M.%20Our%20Synthetic%20Environment.pdf>;
    Bookchin, M. (1971/1986). *Post-Scarcity Anarchism*. Ramparts Press.
    <https://libcom.org/files/Bookchin-Murray-Post-Scarcity-Anarchism-1986.pdf>

[^2]: Hickel, J. (2017). *The Divide: Global Inequality from Conquest to
    Free Markets*. W. W. Norton & Company; Alston, P. (2020). *The
    parlous state of poverty eradication: Report of the Special
    Rapporteur on extreme poverty and human rights*.
    <https://chrgj.org/wp-content/uploads/2020/07/Alston-Poverty-Report-FINAL.pdf>

[^3]: Hickel, J. (2019, 4 February). A letter to Steven Pinker (and Bill
    Gates, for that matter) about global poverty.
    <https://www.jasonhickel.org/blog/2019/2/3/pinker-and-global-poverty>

[^4]: Starrs, S. (2014). The Chimera of Global Convergence. *New Left
    Review*, *87*.
    <https://newleftreview.org/issues/II87/articles/sean-starrs-the-chimera-of-global-convergence>;
    Starrs, S. (under review) *American Power Globalized: Rethinking
    National Power in the Age of Globalization*. Oxford University
    Press.

[^5]: Woodward, D. (2015). *Incrementum ad Absurdum: *Global Growth,
    Inequality, and Poverty Eradication in a Carbon-Constrained World.
    *World Economic Review, 4*, 43-62.
    [https://wer.worldeconomicsassociation.org/files/WEA-WER-4-Woodward.pdf](http://wer.worldeconomicsassociation.org/files/WEA-WER-4-Woodward.pdf)

[^6]: Klein, N. (2014). *This Changes Everything: Capitalism vs. the
    Climate*. Simon & Schuster; Raworth, K. (2017). *Doughnut Economics:
    Seven Ways to Think Like a 21st Century Economist*. Chelsea Green
    Publishing; Vishwas, S. et al. (2018). *The Climate Crisis: South
    African and Global Democratic Eco-Socialist Alternatives*. Wits
    University Press; Hickel, J. (2020). *Less Is More: How Degrowth
    Will Save the World*. Penguin Random House.

[^7]: See Kwet, M. (2019a). Digital Colonialism: U.S. Empire and the New
    Imperialism in the Global South (draft from 2018 free at:
    <https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3232297>);
    Kwet, M. (2019b). Digital Colonialism: South Africa's Education
    Transformation in the Shadow of Silicon Valley. PhD Dissertation,
    Rhodes University.
    <https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3496049>. (For
    a brief overview of other scholarship, see p. 27.) Of course,
    digital colonialism includes "the empire within", where neocolonial
    actors exploit the people inside their own borders.

[^8]: Spencer, K. (2018). *A People's History of Silicon Valley: How the
    Tech Industry Exploits Workers, Erodes Privacy and Undermines
    Democracy*. Eyewear Publishing Ltd.

[^9]: Kwet, M. (2020). *People's Tech for People's Power: A Guide to
    Digital Self-Defense and Empowerment*. Right2Know; Kwet, M. (2019a).
    Op. cit.

[^10]: Kwet, M. (2020, 27 January). The Rise of Smart Camera Networks,
    and Why We Should Ban Them. *The Intercept*.
    <https://theintercept.com/2020/01/27/surveillance-cctv-smart-camera-networks>;
    Kwet, M. (2020, 14 July). The Microsoft Police State: Mass
    Surveillance, Facial Recognition, and the Azure Cloud. *The
    Intercept*.
    <https://theintercept.com/2020/07/14/microsoft-police-state-mass-surveillance-facial-recognition>;
    Robinson, W. (2020). *The Global Police State*. Pluto Press.

[^11]: Kwet, M. (2020, 19 May). To fix social media, we need to
    introduce digital socialism. *Al Jazeera*.
    <https://www.aljazeera.com/indepth/opinion/fix-social-media-introduce-digital-socialism-200512163043881.html>

[^12]: Kwet, M. (2019a). Op. cit.

[^13]: Kwet, M. (2019a). Op. cit.; Tarnoff, B. (2019, 30 November). A
    Socialist Plan to Fix the Internet. *Jacobin*.
    <https://jacobinmag.com/2019/11/tech-companies-antitrust-monopolies-socialist>;
    Kwet, M. (2020, 19 May). Op. cit.

[^14]: Kwet, M. (2020). Op. cit.

[^15]: Kwet, M. (2019a). Op. cit.; Schneider, N. (2020). *Tech New Deal:
    Policies for Community-Owned Platforms*.
    <https://osf.io/t7z2m/?view_only=c8ed9a48a9c04c509c890894d169b206>
    (on financing cooperatively owned platforms).

[^16]: Schmidt, E. (1980). *Decoding Corporate Camouflage: U.S. Business
    Support for Apartheid.* The Institute for Policy Studies.
    [https://kora.matrix.msu.edu/files/50/304/32-130-24F-84-Decoding%20Corporate%20Camouflage%20resized%20opt.pdf](http://kora.matrix.msu.edu/files/50/304/32-130-24F-84-Decoding%20Corporate%20Camouflage%20resized%20opt.pdf)

[^17]: For a short, preliminary overview of this "techlash" circuit, see
    Kwet, M. (2019b). Op. cit.
