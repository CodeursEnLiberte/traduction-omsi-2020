# Pays

## Arabie saoudite

## Argentine

## Australie

## Bangladesh

## Bénin

- transformation numérique pour l'accès à l'information, à la technologie et à
  la santé
- drones, big data et autres pour la cartographie, amélioration de
  l'agriculture, luttre contre le braconnage, protection de zones naturelles
- répercussions négatives des TIC avec les D3E
- critique d'une politique orientée uniquement TIC, mise en avant des
  discriminations structurelles qui se reflètent dans l'accès à la technologie
  mais aussi à la santé et à un environnement non pollué

## Brésil

### 1 

### 2 [traduit]
    - TIC pour la justice sociale, notamment pour les peuples indigènes
    - déforestation et artificialisation vs. défense de territoires naturels et
      de certaines communautés et tribus 

### 3

## Cameroun [traduit]

- urgence climatique dans le pays, exemples détaillés, et aperçu critique de la
  législation s'y rapportant
- agravation des inégalités conséquente
- utilisation et besoins en TIC pour gérer la crise climatique : collecte et
  traitement de données hydrométriques, données géospatiales, météorologiques,
  évaluation des flux de déchets

## Colombie

## Corée du Sud

## Costa Rica

## Congo (République démocratique du)

## Espagne

- économie circulaire, réutilisation d'équipements, recyclage
- inégalités exacerbées pendant les confinements, fracture numérique, livraisons
  retardées de matériel neuf
- modèles économiques de circuits de réparations

## États-Unis d'Amérique

## Éthiopie

## Inde

## Indonésie

## Italie

## Liban

## Malaisie

## Mexique

## Nigéria

## Ouganda [traduit]

- Problème du braconnage
- Programmes de protection des espèces protégées
- Intelligence artificielle pour faire du monitoring

## Pakistan

## Panama

## Roumanie

- pacte vert pour l'Europe
- comparaison du taux et de la qualité d'accès à internet entre pays d'Europe
- comparaison de l'alphabétisation numérique
- droit à la réparation et barrières légales pour que ça s'améliore, recyclage
  des D3E en Roumanie, exemple d'une communauté rurale roumaine
- analyse selon les segments de population, étude empirique des comportements

## Sainte-Lucie

## Seychelles

## Soudan

## Taïwan

- données ouvertes pour une agriculture durable
- aide à la conversion vers une agriculture alternative
- mélange données ouvertes, big data, IA, mise à disposition des petits
  agriculteurs

## Tunisie

- amélioration de l'utilisation de l'eau avec les technologies intelligentes

## Turquie

- impact des confinements en Turquie sur l'activisme, notamment en ligne,
  réponses gouvernementales
- violences faites aux femmes et Convention d'Istanbul, invisibilisation et
  augmentation des violences du fait des confinements
- inégalités d'accès à l'éducation en période confinée (fracture numérique)
- lois restreignant les libertés sur internet

## Vénézuéla

## Zimbabwe

# Régionaux

## Afrique

### 1

- intersection entre mobilisatio de la jeunesse pour le climat, de leur
  utilisation des TIC pour diffuser leurs message, et de l'utilisation des TIC
  dans des actions de conservation et lutte contre le réchauffement par des
  jeunes
- émetteurs GPS sur des oiseaux protégés en Éthiopie et utilisation des données
  à des fins pédagogiques et de sensibilisation dans des écoles de régions
  défavorisées
- coordination de communauté avec Whatsapp face à une catastrophe écologique
  pour des oiseaux protégés au Kenya
- jeunesse pour la biodiversité en Tanzanie, coordination par internet, outils
  d'éducation et partage de connaissance avec des universités

### 2

- TIC comme catalyseuses de l'économie verte en Afrique australe
- pénétration des TIC dans les pays d'Afrique australe, indicateurs de
  développement (PIB etc.)
- création d'emplois verts grâce aux TIC

## Amérique latine

### 1

### 2

- réseaux communautaires, connexion de communautés rurales et/ou exclues,
  utilisation en soutien aux peuples indigènes et autonomes
- conséquences de la déforestation sur les communautés et la possibilité de
  continuer à vivre
- retour d'expériences sur tentatives d'essaimage du modèle vers d'autres pays

### 3

## Asie

## Europe

- effet de la nouvelle e-justice sur les droits relatifs à l'environnement
- contexte de la convention d'Aarhus (pour les droits environnementaux)
- exemples de fonctionnement (et problèmes) de la e-justice en Hongrie,
  Autriche, Estonie, Roumanie, Slovénie, Bulgarie

# Thématiques

## Réseaux communautaires

## Principes féministes

## TIC et l'environnement [traduit]

- amélioration de l'efficacité énergétique des appareils vs. augmentation de la
  consommation
- production de D3E, de GES, droit à la réparation
- montée des populismes, délocalisation de l'emploi, augmentation des
  inégalités, uberisation
- OSM, Ushahidi, Sahana, données géospatiales ouvertes
- pacte vert vs. vie entièrement numérisée

## Peuples indigènes

## Paradoxe écologiques des économies numériques [traduit]

- amélioration de l'efficacité vs. augmentation de la consommation
- D3E
- « extractivisme vert » et colonialisme afférent lié au capitalisme
- décroissance

## Objectifs de développement durable
