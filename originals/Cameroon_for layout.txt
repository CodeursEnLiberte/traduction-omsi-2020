**Cameroon**

**Authors: Serge Daho, Emmanuel Bikobo and Avis Momeni**

**Organisation: PROTEGE QV**

**URL: [www.protegeqv.org](http://www.protegeqv.org)**

**Title: The necessary assistance from the countries of the North to
help curb the harmful effects of global warming in Cameroon**

**Introduction**

As anticipated by the National Observatory on Climate Change[^1] in its
seasonal bulletin, Cameroon experienced excess rainfall both in June and
July 2020. In addition to these heavy rains, the Observatory had also
predicted landslides and cholera cases in several regions of the
country. These are some of the manifestations of extreme weather events
that the country is facing as a result of climate change.

With its 475,000 square kilometres, Cameroon lies in the heart of West
Central Africa and prides itself for containing within its territory all
the natural resources which are dispersed throughout the rest of the
continent. In addition to this, all of Africa's major climates can also
be found in the country. Yet, its 27 million inhabitants[^2] are facing
a major threat: the worldwide climate and environmental emergency.

Arguably, Africa is the continent most vulnerable to climate change.
Fortunately, information and communications technologies (ICTs), which
are a cross-cutting technology, hold the potential to address climate
change by helping countries to adapt to its effects, and to mitigate its
impact.

This report outlines challenges posed by climate change in Cameroon and
existing ICT initiatives that aim to address them. The analysis is based
upon a review of literature and data and supported by the interview of a
key governmental official.

**The national context**

As outlined by the Cameroonian head of state in an open forum titled
"**Time for Action"**[^3] shortly before the 2015 United Nations Climate
Change Conference in Paris (COP21), the context of this report is a
context of climate emergency. Cameroon is facing an environmental crisis
that is likely to profoundly affect its ecosystems and economic sectors
(such as agriculture, forestry and tourism) as well as infrastructures
(dams, roads, water and sanitation) and areas related to human
development (such as health, education, employment).

**Legislative and regulatory framework**

Cameroon has ratified more than 23 international agreements in the
fields of forests and the environment.[^4] Among the conventions
ratified by the country are:

-   The United Nations Framework Convention on Climate Change
    (UNFCCC) (1994)

-   The Brazzaville Treaty on the Conservation and Sustainable
    Management of Forest Ecosystems in Central Africa (2005).

With respect to climate change, Cameroon is party to the following
agreements and protocols:

-   The Paris Agreement (2016)[^5]

-   The Kyoto Protocol (2002).[^6]

The country also has a number of laws and regulations on the environment
and sustainable development. The following laws, decrees and orders are
worth mentioning:

-   Law No. 96/12 of 5 August 1996, or the framework law on
    environmental management; this law lays down the general legal
    regime for environmental management in Cameroon.

-   Law No. 2016/008 of 12 July 2016, authorising the President of the
    Republic to ratify the Paris Climate Agreement adopted in Paris on
    12 December 2015 and signed in New York on 22 April 2016.

-   Decree No. 2004/320 of 8 December 2004, creating the Ministry of
    Environment, Protection of Nature and Sustainable Development
    (MINEPDED).The creation of this ministry is linked to the desire of
    the government of Cameroon to work effectively in the creation of a
    healthy environment for the well-being of the population as a whole.
    To this end, the mission of the ministry is to develop, implement
    and monitor environmental policy and mechanisms aimed at the
    protection of nature in Cameroon.[^7]

-   Order No. 100/PM of 11 July 2006, creating an inter-ministerial
    committee on the environment. The purpose of this committee is to
    facilitate the implementation of the sectoral programme on forests
    and the environment.

However, these texts are almost all related to the management of natural
resources, such as the law on the environment, the law on forests and
the laws on water, mines, and gas. In comparison, the actual regulatory
framework for dealing with climate change looks a bit sketchy:

-   In 1999, Cameroon developed its **First National Communication**
    marking its accession to the UNFCCC.

-   Decree No. 2009/410 of 10 December 2009, creating the National
    Observatory on Climate Change, well known under the French acronym
    ONACC. *Inter alia*, the Observatory has to collect, analyse and
    make available the reference data on climate change in Cameroon to
    public and private decision makers as well as various national and
    international bodies;

```{=html}
<!-- -->
```
-   June 2015 saw the adoption of the first **National Adaptation Plan
    for Climate Change (NAPCC).** The primary objectives of the NAPCC
    are to reduce carbon emissions to slow down global warming, and to
    implement climate change adaptation measures to minimise the damage
    of unavoidable climate-related disasters.

**Cameroon\'s bleak climate change outlook**

Cameroon is a resource-rich country heavily dependent on revenues
generated from oil, timber and agricultural products[^8] for continued
development at both local and national levels. Therefore, the country is
highly vulnerable to the impacts of climate change. Cameroon is divided
into three main climatic zones:

-   The equatorial zone which extends from the second to the sixth
    degree North latitude. It is characterised by abundant rainfall
    reaching an annual average of 2,000 mm of rainfall. The average
    temperature is around 25° Celsius.

-   The Sudanian zone extends from the seventh to the tenth degree North
    latitude. The dry season here lasts five to six months. The average
    temperature is 22° Celsius, and 1,000 mm of rainfall is observed
    during the year.

-   The Sudano-Sahelian zone, which extends beyond the tenth degree
    North latitude. It is characterised by a seven-month dry season and
    low rainfall.

However, the climate changes observed in the country have made
predicting trends more complex, and only regular weather measurements at
observation posts can provide reliable data on climatic conditions.

For Cameroon, agriculture is important. The agriculture and forestry
sectors provide employment for the majority of the population. About 80%
of the country's poor live in rural areas and work primarily in
agriculture. About 35% of Cameroon's GDP comes from agriculture and
related activities. The agricultural system is highly dependent on the
climate, because temperature, sunlight and water are the main drivers of
crop growth.[^9] Therefore, the effects of global warming and climate
change on the agricultural sector are likely to threaten both the
welfare of the population and the economic development of the country.
Furthermore, the country's susceptibility to weather pattern changes
could be extended to sectors like forestry and tourism, as well as to
infrastructures (dams, roads, water and sanitation), and areas related
to human development (such as health, education and employment).

The frequency of extreme weather conditions across the country, "false
starts" to seasons, the recent deadly floods due to heavier rainfall
(2013, 2015 and 2017 in the Far North region), and recurrent droughts
that have scorched large expanses of land allowing the desert to
advance, have already resulted in the migration of local populations in
search of water and arable land. The same applies to fishers and herders
who have also migrated to other areas after being affected by shortened
rainy seasons and an increase in temperature.

There have also been landslides and mudslides, particularly in Bafaka
and Limbe (in the South West region, in 1997, 1998, 2001 and 2003), and
in Yaounde (in the central region, in 1998 and 2019).

As temperatures continue to rise, these impacts of climate change are
expected to become more profound throughout the country. Average annual
temperatures are predicted to increase between 1.5°C and 4.5°C by 2100
with a 1.6°C to 3.3°C rise in coastal zones and a 2.1°C to 4.5°C rise in
the Sudano-Sahelian region.[^10] Therefore, it is essential that
Cameroon increase its capacity in all areas, particularly human and
technical capacity to respond. This will necessarily involve the use of
ICTs.

**Cameroon\'s responses to global warming through the use of ICTs**

ICTs encompass devices and services that enable the reception,
transmission and display of data and information in electronic form, and
are part of the solutions for adapting to and mitigating the effects of
climate change. This is a key challenge to a country such as Cameroon.
Do we have the necessary infrastructure, financial and human capacity to
cope with the global warming using ICTs?

As the president stated prior to COP21: "For a country like Cameroon,
the reduction of greenhouse gases remains conditional on support from
the international community \[\...\] in the form of financing, capacity
building and technology transfer.\"[^11]

Table 1 outlines the technology-related infrastructure in place in
Cameroon to tackle the harmful effects of the climate change.

**Table 1. Use of ICTs to confront climate change in Cameroon**

+----------------------------------+----------------------------------+
| Action                           | ICT tools used                   |
+==================================+==================================+
| **Environmental observation,     | A fairly well-structured         |
| climate monitoring and climate   | national hydrometric network     |
| change prediction**              | made up of hydrometric stations  |
|                                  | distributed over the five major  |
|                                  | river basins in Cameroon (Lake   |
|                                  | Chad, Niger, Sanaga, Congo and   |
|                                  | Nyong and Coastal Rivers). It    |
|                                  | also relies on the Global        |
|                                  | Observing System of the World    |
|                                  | Meteorological Organization.     |
+----------------------------------+----------------------------------+
| **Data processing systems**      | The software for computer        |
|                                  | processing of hydro-pluviometric |
|                                  | data (TIDHYP) developed at the   |
|                                  | Centre de Recherche Hydrologique |
|                                  | is used.                         |
|                                  |                                  |
|                                  | Statistical software such as     |
|                                  | HYFRAN and other software        |
|                                  | (XlStat, Excel, KhronoStat,      |
|                                  | etc.) is necessary for modelling |
|                                  | and/or forecasting.              |
+----------------------------------+----------------------------------+
| **Research and development       | Use of satellite images (raw     |
| activities**                     | images and products derived from |
|                                  | the AMESD/MESA programme of the  |
|                                  | Meteosat Second Generation       |
|                                  | satellite).                      |
|                                  |                                  |
|                                  | Use of remote sensing software   |
|                                  | and geographic information       |
|                                  | systems (GIS).                   |
+----------------------------------+----------------------------------+
| **Installation of pyranometers** | In 2011 the Laboratoire de       |
|                                  | Recherches Energétiques          |
|                                  | proceeded with the installation  |
|                                  | of pyranometers. The objective   |
|                                  | is to monitor the impact on the  |
|                                  | climate by collecting            |
|                                  | measurements for solar           |
|                                  | radiation, air humidity,         |
|                                  | rainfall, wind direction and     |
|                                  | speed, barometric pressure, as   |
|                                  | well as ultraviolet radiation.   |
+----------------------------------+----------------------------------+
| **Climate change and integrated  | An ICT tool for assessing waste  |
| waste management**               | flows and types is used here by  |
|                                  | the ONACC. The software is       |
|                                  | provided by the                  |
|                                  | Intergovernmental Panel on       |
|                                  | Climate Change (IPCC).[^13]      |
+----------------------------------+----------------------------------+

**Conclusion**

Observed changes or expected changes to the climate and weather are a
matter of great concern in Cameroon, but the country seems to have low
adaptive capacity that is further compounded by the poor connections
between different levels of government and the various communities.
Climate change is exacerbating already entrenched poverty which prevails
at the grassroots level. Because of this, the contribution of ICTs is
necessary to try to reverse this trend, as is called for in the Bali
Action Plan. But the limited financial resources in the country make
both adaptation and mitigation through the use of ICTs extremely
difficult. As a result, multifaceted assistance from industrialised
countries will be more than welcome for Cameroon.

**Action steps**

**The following steps are a priority in Cameroon:**

-   Make the fight against global warming one of the top concerns of
    both the government as a whole and the ministry in charge of the
    environment (MINEPDED). So far this is not the case, as global
    warming does not even feature in their priorities. This would
    include the government taking climate change into account in its
    operational policies.

-   Strengthen coordination between the various ministries and bodies in
    charge of the environment in Cameroon. This weak coordination is at
    the heart of the failure to take climate change into account in the
    multi-sectoral forest and environment committee created in 2006.

-   The country must upgrade its nearly 31 climatological stations and
    400 rainfall stations, allowing them to play a much more effective
    role in monitoring the weather.

-   The capacities of the different actors in the fight against climate
    change must be strengthened for better understanding and use of
    monitoring data.

-   ICTs should be embraced through technology transfer as a means of
    climate change adaptation and mitigation, as called for in Article
    4.5 of the UNFCCC.

[^1]: The National Observatory on Climate Change was established in 2009
    to help seek solutions to the negative impacts of climate change
    affecting the agriculture, transport, industrial and other sectors.
    It produces regular bulletins with forecasts on seasonal climatic
    conditions to facilitate adaptation activities in the different
    sectors of the economy. <https://onacc.cm/about>

[^2]: [https://www.worldometers.info/world-population/cameroon-population](https://www.worldometers.info/world-population/cameroon-population/)

[^3]: <https://www.prc.cm/fr/actualites/discours/1568-tribune-libre-du-chef-de-l-etat-s-e-paul-biya>

[^4]: <http://www.transparenceforestiere.info/cameroon/2010/themes/3/16>

[^5]: The Paris Agreement is an agreement within the United Nations
    Framework Convention on Climate Change dealing with greenhouse gas
    emissions mitigation, adaptation and finance.
    <https://en.wikipedia.org/wiki/Paris_Agreement>

[^6]: The **Kyoto Protocol** is an international
    [treaty](https://en.wikipedia.org/wiki/Treaty) which extends the
    1992 [United Nations Framework Convention on Climate
    Change](https://en.wikipedia.org/wiki/United_Nations_Framework_Convention_on_Climate_Change)
    that commits state parties to reduce [greenhouse
    gas](https://en.wikipedia.org/wiki/Greenhouse_gas) emissions, based
    on the [scientific
    consensus](https://en.wikipedia.org/wiki/Scientific_opinion_on_climate_change)
    that (part one) [global
    warming](https://en.wikipedia.org/wiki/Global_warming) is occurring
    and (part two) it is extremely likely that human-made [CO~2~
    emissions](https://en.wikipedia.org/wiki/Carbon_dioxide_in_Earth%27s_atmosphere)
    have predominantly caused it. The Kyoto Protocol was adopted in
    [Kyoto](https://en.wikipedia.org/wiki/Kyoto),
    [Japan](https://en.wikipedia.org/wiki/Japan), on 11 December 1997
    and entered into force on 16 February 2005.
    <https://en.wikipedia.org/wiki/Kyoto_Protocol>

[^7]: <https://minepded.gov.cm/en>

[^8]: Norrington-Davies, G. (2011). *Climate Change Financing and Aid
    Effectiveness: Cameroon Case Study*. OECD/DAC & African Development
    Bank.
    <https://www.oecd.org/environment/environment-development/48458409.pdf>

[^9]: **Molua, E. L., & Lambi, C. M. (2007). The Economic Impact of
    Climate Change on Agriculture in Cameroon.The World Bank.
    <https://openknowledge.worldbank.org/handle/10986/7362>**

[^10]: Norrington-Davies, G. (2011).Op. cit.

[^11]: <https://www.prc.cm/fr/actualites/discours/1568-tribune-libre-du-chef-de-l-etat-s-e-paul-biya>

[^12]: Interview with Dr. Joseph Armathé Amougou, director general of
    the National Observatory on Climate Change (ONACC).
    <https://onacc.cm>

[^13]: Interview with Dr. Joseph Armathé Amougou, director general of
    the National Observatory on Climate Change (ONACC).
    <https://onacc.cm>
